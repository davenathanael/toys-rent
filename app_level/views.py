from django.shortcuts import render
from django.db import connection

# Create your views here.

def daftar_level(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_level, deskripsi, minimum_poin FROM LEVEL_KEANGGOTAAN")
        all_levels = cursor.fetchall()

        counter = 1
        for row in all_levels:
            row += (counter,)
            counter += 1
    response['daftar_level'] = all_levels
    return render(request, 'daftar-level.html', response)

def tambah_level(request):
    response = {}
    return render(request, 'tambah-level.html', response)

def update_level(request):
    response = {}
    return render(request, 'update-level.html', response)