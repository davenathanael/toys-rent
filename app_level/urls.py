from django.conf.urls import url
from django.urls import path
from .views import tambah_level, daftar_level, update_level


app_name = "app_level"


urlpatterns = [
    path('', daftar_level, name='daftar_level'),
    path('tambah-level/', tambah_level, name='tambah_level'),
    path('update-level/', update_level, name='update_level'),
]