from django.conf.urls import url
from django.urls import include
from .views import index, form_create_pengiriman, form_update_pengiriman

app_name = 'app_pengiriman'

urlpatterns = [
    url(r'^$', index, name='list_pengiriman'),
    url(r'form-create/$', form_create_pengiriman, name='form_create_pengiriman'),
    url(r'form-update/$', form_update_pengiriman, name='form_update_pengiriman'),
]
