from django.shortcuts import render
from django.db import connection

# Create your views here.
response = {}

def index(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PENGIRIMAN")
        record = cursor.fetchall()
    response['pengiriman'] = record

    return render(request, 'list_pengiriman.html', response)

def form_create_pengiriman(request):
    return render(request, 'form_create_pengiriman.html', response)

def form_update_pengiriman(request):
    return render(request, 'form_update_pengiriman.html', response)
