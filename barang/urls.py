from django.urls import path
from .views import index, detail, update, create, delete

app_name = 'barang'

urlpatterns = [
    path('', index, name='index'),
    path('detail/<str:id>', detail, name='detail'),
    path('update/<str:id>', update, name='update'),
    path('delete/<str:id>', delete, name='delete'),
    path('create', create, name='create')
]
