from django.shortcuts import render, redirect
from django.db import connection

def index(req):
    if "no_ktp" not in req.session.keys():
        return redirect("/auth/login")
    query = "SELECT * FROM BARANG"
    sortParam = req.GET.get('sort')
    filterParam = req.GET.get('filter')
    sortQuery = ""
    byKategori = False

    if filterParam != None:
        query = "SELECT B.* FROM BARANG B, KATEGORI_ITEM K WHERE K.nama_item=B.nama_item AND K.nama_kategori='{}'".format(filterParam)

    if sortParam != None:
        if (sortParam == 'namaAsc'):
            sortQuery = " ORDER BY nama_item ASC"
        elif (sortParam == 'namaDesc'):
            sortQuery = " ORDER BY nama_item DESC"
        elif (sortParam == 'kategoriAsc'):
            byKategori = True
            sortQuery = " ORDER BY nama_kategori ASC"
        elif (sortParam == 'kategoriDesc'):
            byKategori = True
            sortQuery = " ORDER BY nama_kategori DESC"
    
    if byKategori and "KATEGORI_ITEM" not in query:
        query = "SELECT B.* FROM BARANG B, KATEGORI_ITEM K"

    query = query + sortQuery

    with connection.cursor() as cursor:
        cursor.execute(query)
        barang = cursor.fetchall()
        cursor.execute("SELECT DISTINCT K.nama_kategori FROM BARANG B, KATEGORI_ITEM K WHERE B.nama_item=k.nama_item")
        kategori = cursor.fetchall()
    res = {
        'kategori': kategori,
        'barang': barang,
        'is_admin': req.session['is_admin']
    }
    return render(req, 'daftarBarang.html', res)

def detail(req, id):
    if "no_ktp" not in req.session.keys():
            return redirect("/auth/login")
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM BARANG WHERE id_barang='{0}'".format(id))
        barang = cursor.fetchone()
        cursor.execute("SELECT review FROM BARANG_DIKIRIM WHERE id_barang='{0}'".format(id))
        review = cursor.fetchall()
    res = {
        'barang': barang,
        'review': review
    }
    return render(req, 'detailBarang.html', res)

def update(req, id):
    if "no_ktp" not in req.session.keys():
        return redirect("/auth/login")
    if not req.session["is_admin"]:
        return redirect("/barang")

    if req.method == 'POST':
        id_barang = req.POST.get('idBarang')
        nama_item = req.POST.get('namaItem')
        warna = req.POST.get('warna')
        url_foto = req.POST.get('urlFoto')
        kondisi = req.POST.get('kondisi')
        lama_penggunaan = req.POST.get('lamaPenggunaan')
        no_ktp_penyewa = req.POST.get('penyewa')

        level = ['BRONZE', 'SILVER', 'GOLD', 'PLATINUM']
        level_form = []

        for l in level:
            level_form.append({
                'harga_sewa': req.POST.get('sewa{0}'.format(l)),
                'porsi_royalti': req.POST.get('persenRoyalty{0}'.format(l))
            })

        with connection.cursor() as cursor:
            cursor.execute("UPDATE BARANG SET id_barang='{}', nama_item='{}', warna='{}', url_foto='{}', kondisi='{}', lama_penggunaan={}, no_ktp_penyewa='{}' WHERE id_barang='{}'".format(
                id_barang,
                nama_item,
                warna,
                url_foto,
                kondisi,
                int(lama_penggunaan),
                no_ktp_penyewa,
                id_barang
            ))

            for i in range(len(level)):
                if level_form[i].get('harga_sewa') != None and level_form[i].get('porsi_royalti') != None:
                    cursor.execute("UPDATE INFO_BARANG_LEVEL SET harga_sewa={}, porsi_royalti={} WHERE id_barang='{}' AND nama_level='{}'".format(
                        float(level_form[i]['harga_sewa']),
                        float(level_form[i]['porsi_royalti']),
                        id_barang,
                        level[i],
                    ))
        return redirect('/barang/detail/{0}'.format(id))
    if req.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM BARANG WHERE id_barang='{0}'".format(id))
            record = cursor.fetchone()
            cursor.execute("SELECT nama FROM ITEM")
            item = cursor.fetchall()
            cursor.execute("SELECT no_ktp FROM ANGGOTA")
            penyewa = cursor.fetchall()
            cursor.execute("SELECT * from INFO_BARANG_LEVEL WHERE id_barang='{0}'".format(id))
            info = cursor.fetchall()

        res = {
            'barang': record,
            'item': item,
            'penyewa': penyewa,
            'info': info
        }
    return render(req, 'updateBarang.html', res)

def create(req):
    if "no_ktp" not in req.session.keys():
        return redirect("/auth/login")
    if not req.session["is_admin"]:
        return redirect("/barang")

    if req.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT nama FROM ITEM")
            item = cursor.fetchall()
            cursor.execute("SELECT no_ktp FROM ANGGOTA")
            penyewa = cursor.fetchall()
            cursor.execute("SELECT nama_level from LEVEL_KEANGGOTAAN")
            info = cursor.fetchall()
        res = {
            'item': item,
            'penyewa': penyewa,
            'info': info
        }
        return render(req, 'createBarang.html', res)
    if req.method == 'POST':
        id_barang = req.POST.get('idBarang')
        nama_item = req.POST.get('namaItem')
        warna = req.POST.get('warna')
        url_foto = req.POST.get('urlFoto')
        kondisi = req.POST.get('kondisi')
        lama_penggunaan = req.POST.get('lamaPenggunaan')
        no_ktp_penyewa = req.POST.get('penyewa')

        level = ['BRONZE', 'SILVER', 'GOLD', 'PLATINUM']
        level_form = []

        for l in level:
            level_form.append({
                'harga_sewa': req.POST.get('sewa{0}'.format(l)),
                'porsi_royalti': req.POST.get('persenRoyalty{0}'.format(l))
            })

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO BARANG VALUES ('{}', '{}', '{}', '{}', '{}', {}, '{}')".format(
                id_barang,
                nama_item,
                warna,
                url_foto,
                kondisi,
                int(lama_penggunaan),
                no_ktp_penyewa
            ))

            for i in range(len(level)):
                cursor.execute("INSERT INTO INFO_BARANG_LEVEL VALUES ('{}', '{}', {}, {})".format(
                    id_barang,
                    level[i],
                    float(level_form[i]['harga_sewa']),
                    float(level_form[i]['porsi_royalti'])
                ))
        return redirect('/barang')

def delete(req, id):
    if "no_ktp" not in req.session.keys():
        return redirect("/auth/login")
    if not req.session["is_admin"]:
        return redirect("/barang")

    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM BARANG WHERE id_barang='{0}'".format(id))
    return redirect('/barang')
