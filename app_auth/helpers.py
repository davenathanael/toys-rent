from django.shortcuts import redirect


def login_required(f):
    def g(request):
        if "no_ktp" not in request.session.keys():
            return redirect("/auth/login")
        return f(request, request.session["no_ktp"], request.session["is_admin"])

    return g


def admin_required(f):
    def g(request):
        if "no_ktp" not in request.session.keys():
            return redirect("/auth/login")
        if not request.session["is_admin"]:
            return redirect("/barang")
        return f(request, request.session["no_ktp"])

    return g
