from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def login(request):
    if request.method == 'GET':
        if "is_admin" in request.session.keys():
            if request.session['is_admin']:
                return redirect("/pemesanan")
            else:
                return redirect('/barang')
            
        return render(request, 'auth/login.html')

    no_ktp = request.POST.get("no_ktp")
    email = request.POST.get("email")

    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM PENGGUNA WHERE no_ktp = %s AND email = %s", [no_ktp, email]
    )
    
    is_valid = len(cursor.fetchall()) > 0
    if not is_valid:
        return render(request, 'auth/login.html', {"error": "Invalid login, please try again"})

    cursor.execute(
        "SELECT * FROM ADMIN WHERE no_ktp = %s", [no_ktp]
    )
    is_admin = len(cursor.fetchall()) > 0

    request.session['no_ktp'] = no_ktp
    request.session['email'] = email
    request.session['is_admin'] = is_admin

    if is_admin:
        return redirect("/pemesanan")
    else:
        return redirect("/barang")


def logout(request):
    for key in list(request.session.keys()):
        del request.session[key]

    return redirect('/auth/login')


def signup(request):
    if "is_admin" in request.session.keys():
        if request.session['is_admin']:
            return redirect("/pemesanan")
        else:
            return redirect('/barang')

    return render(request, 'auth/signup.html', {})


def signup_admin(request):
    if request.method == 'POST':
        no_ktp = request.POST.get("no_ktp")
        nama = request.POST.get("nama")
        email = request.POST.get("email")
        tanggal_lahir = request.POST.get("tanggal_lahir")
        no_telp = request.POST.get("no_telp")

        error = {}
        if not no_ktp:
            error["no_ktp"] = "This field cannot be empty"
        if not nama:
            error["nama"] = "This field cannot be empty"
        if not email:
            error["email"] = "This field cannot be empty"
        if error:
            return render(request, 'auth/signup_admin.html', {'error' : error})

        if not tanggal_lahir:
            tanggal_lahir = None
        if not no_telp:
            no_telp = None
        if not nama:
            nama = None

        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE no_ktp = %s", [no_ktp]
        )
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["no_ktp"] = "Ktp is already registered"

        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE email = %s", [email]
        )
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["email"] = "Email is already registered"

        if error:
            return render(request, 'auth/signup_admin.html', {'error' : error})

        cursor.execute(
            """INSERT INTO PENGGUNA (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) 
            VALUES (%s, %s, %s, %s, %s)""",
            [no_ktp, nama, email, tanggal_lahir, no_telp]
        )
        cursor.execute(
            "INSERT INTO ADMIN (no_ktp) VALUES (%s)", [no_ktp]
        )

        return render(request, 'auth/login.html')


    if "is_admin" in request.session.keys():
        if request.session['is_admin']:
            return redirect("/pemesanan")
        else:
            return redirect('/barang')

    return render(request, 'auth/signup_admin.html')


def signup_anggota(request):
    if request.method == 'POST':
        no_ktp = request.POST.get("no_ktp")
        nama = request.POST.get("nama")
        email = request.POST.get("email")
        tanggal_lahir = request.POST.get("tanggal_lahir")
        no_telp = request.POST.get("no_telp")

        nama_alamat = request.POST.get("nama_alamat")
        jalan = request.POST.get("jalan")
        nomor = request.POST.get("nomor")
        kota = request.POST.get("kota")
        kodepos = request.POST.get("kodepos")
        print(nama_alamat, jalan, nomor, kota, kodepos)
        error = {}
        if not no_ktp:
            error["no_ktp"] = "This field cannot be empty"
        if not nama:
            error["nama"] = "This field cannot be empty"
        if not email:
            error["email"] = "This field cannot be empty"
        if not (nama_alamat and jalan and nomor and kota and kodepos):
            error["alamat"] = "All address fields are required"
        if error:
            return render(request, 'auth/signup_anggota.html', {'error' : error})

        if not tanggal_lahir:
            tanggal_lahir = None
        if not no_telp:
            no_telp = None
        if not nama:
            nama = None

        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE no_ktp = %s", [no_ktp]
        )
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["no_ktp"] = "Ktp is already registered"

        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE email = %s", [email]
        )
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["email"] = "Email is already registered"

        if error:
            return render(request, 'auth/signup_anggota.html', {'error' : error})

        cursor.execute(
            """INSERT INTO PENGGUNA (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) 
            VALUES (%s, %s, %s, %s, %s)""",
            [no_ktp, nama, email, tanggal_lahir, no_telp]
        )
        cursor.execute(
            "INSERT INTO ANGGOTA (no_ktp, poin, level) VALUES (%s, %s, %s)", [no_ktp, 0, "BRONZE"]
        )
        cursor.execute(
            """INSERT INTO ALAMAT (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) 
            VALUES (%s, %s, %s, %s, %s, %s)""",
            [no_ktp, nama_alamat, jalan, nomor, kota, kodepos]
        )

        return render(request, 'auth/login.html')


    if "is_admin" in request.session.keys():
        if request.session['is_admin']:
            return redirect("/pemesanan")
        else:
            return redirect('/barang')
            
    return render(request, 'auth/signup_anggota.html')
