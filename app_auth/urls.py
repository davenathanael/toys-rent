from django.conf.urls import url
from django.urls import include
from .views import login, logout, signup, signup_admin, signup_anggota

urlpatterns = [
    url(r'^login$', login, name='auth'),
    url(r'^logout$', logout, name='auth'),
    url(r'^signup$', signup, name='auth'),
    url(r'^signup_admin$', signup_admin, name='auth'),
    url(r'^signup_anggota$', signup_anggota, name='auth'),
]
