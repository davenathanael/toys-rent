from django.conf.urls import url
from django.urls import include
from .views import index


urlpatterns = [
    url(r'^$', index, name='profil'),
]
