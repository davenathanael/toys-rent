from django.shortcuts import render
from django.db import connection
from app_auth.helpers import login_required

# Create your views here.
response = {}

@login_required
def index(request, no_ktp, is_admin):
    if is_admin:
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp = %s", [no_ktp])
            record = cursor.fetchall()
        response["is_admin"] = True
    else:
        with connection.cursor() as cursor:
            cursor.execute("""SELECT * FROM PENGGUNA NATURAL JOIN ANGGOTA
            WHERE no_ktp = %s""", [no_ktp]
            )
            record = cursor.fetchall()
        response["is_admin"] = False

    response['penggunas'] = record

    return render(request, 'profil/profil.html', response)