from django.apps import AppConfig


class AppPesananConfig(AppConfig):
    name = 'app_pesanan'
