from django.conf.urls import url
from django.urls import path
from .views import form_pemesanan, daftar_pemesanan, update_pemesanan


app_name = "app_pesanan"


urlpatterns = [
    path('', daftar_pemesanan, name='daftar_pemesanan'),
    path('pesan/', form_pemesanan, name='form_pemesanan'),
    path('update/', update_pemesanan, name='update_pemesanan'),
]