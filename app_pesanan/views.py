from django.shortcuts import render
from django.db import connection

# Create your views here.

def daftar_pemesanan(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_pemesanan, (harga_sewa + ongkos), status FROM PEMESANAN")
        all_orders_record = cursor.fetchall()

        counter = 0
        for row in all_orders_record:
            cursor.execute("SELECT b.nama_item from barang as b, barang_pesanan as d, pemesanan as p where b.id_barang = d.id_barang AND d.id_pemesanan = p.id_pemesanan AND p.id_pemesanan = \'{}\'".format(row[0]))
            order_item_record = cursor.fetchall() # a list of tuples of single string (what a waste!)
            order_items_tuple = tuple() # oh no I forget to implement clean code
            for item in order_item_record:
                order_items_tuple += item # take the only element from each tuple and add to a new tuple
            all_orders_record[counter] += (order_items_tuple,)
            counter += 1
    response['daftar_pemesanan'] = all_orders_record
    return render(request, 'daftar-pemesanan.html', response)

def form_pemesanan(request):
    response = {}
    return render(request, 'form-pemesanan.html', response)

def update_pemesanan(request):
    response = {}
    return render(request, 'update-pemesanan.html', response)