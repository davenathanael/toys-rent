--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: update_for_new_level(); Type: FUNCTION; Schema: public; Owner: db2018013
--

CREATE FUNCTION public.update_for_new_level() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    row RECORD;
BEGIN
    FOR row in SELECT * FROM ANGGOTA
LOOP
UPDATE ANGGOTA
SET poin = row.poin
WHERE no_ktp = row.no_ktp;
END LOOP;

RETURN NEW;
END;
$$;


-- ALTER .* OWNER TO db2018013;

--
-- Name: update_harga_sewa(); Type: FUNCTION; Schema: public; Owner: db2018013
--

CREATE FUNCTION public.update_harga_sewa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
harga_sewa_baru INTEGER;

BEGIN
SELECT IBL.harga_sewa
FROM INFO_BARANG_LEVEL IBL, BARANG B, 
LEVEL_KEANGGOTAAN LK, PEMESANAN P, ANGGOTA A
WHERE NEW.id_barang = B.id_barang
AND IBL.id_barang = B.id_barang
AND NEW.id_pemesanan = P.id_pemesanan
AND P.no_ktp_pemesan = A.no_ktp
AND A.level = LK.nama_level
AND IBL.nama_level = LK.nama_level
INTO harga_sewa_baru;

UPDATE PEMESANAN
SET harga_sewa = harga_sewa + harga_sewa_baru
WHERE id_pemesanan = NEW.id_pemesanan;
RETURN NEW;

END;
$$;


-- ALTER .* OWNER TO db2018013;

--
-- Name: update_kondisi_barang(); Type: FUNCTION; Schema: public; Owner: db2018013
--

CREATE FUNCTION public.update_kondisi_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            UPDATE BARANG
            SET kondisi = 'sedang disewa anggota'
            WHERE id_barang = NEW.id_barang;
        END IF;
        RETURN NEW;
    END;
$$;


-- ALTER .* OWNER TO db2018013;

--
-- Name: update_level(); Type: FUNCTION; Schema: public; Owner: db2018013
--

CREATE FUNCTION public.update_level() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
NEW.level := (
SELECT L.nama_level FROM LEVEL_KEANGGOTAAN L
WHERE L.minimum_poin IN (
SELECT MAX(K.minimum_poin) FROM LEVEL_KEANGGOTAAN K
WHERE K.minimum_poin <= NEW.poin));
RETURN NEW;
END;
$$;


-- ALTER .* OWNER TO db2018013;

--
-- Name: update_ongkos(); Type: FUNCTION; Schema: public; Owner: db2018013
--

CREATE FUNCTION public.update_ongkos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN
UPDATE PEMESANAN
SET ongkos = ongkos + NEW.ongkos
WHERE id_pemesanan = NEW.id_pemesanan;

RETURN NEW;
END;

$$;


-- ALTER .* OWNER TO db2018013;

--
-- Name: update_poin(); Type: FUNCTION; Schema: public; Owner: db2018013
--

CREATE FUNCTION public.update_poin() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        raise notice 'Masuk trig w/ TABLE: %', TG_TABLE_NAME;
        IF (TG_OP = 'INSERT') AND (TG_TABLE_NAME = 'pengiriman') THEN
            raise notice 'Masuk if %', TG_TABLE_NAME;
            UPDATE ANGGOTA
            SET poin = poin + 100
            WHERE no_ktp = NEW.no_ktp_anggota;
        END IF;
        
        IF (TG_OP = 'INSERT') AND (TG_TABLE_NAME = 'barang') THEN
            UPDATE ANGGOTA
            SET poin = poin + 100
            WHERE no_ktp = NEW.no_ktp_penyewa;
        END IF;
        RETURN NEW;
    END;
$$;


-- ALTER .* OWNER TO db2018013;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.admin (
    no_ktp character varying(20) NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: alamat; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(255) NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: anggota; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: barang; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20) NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: barang_dikirim; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: barang_pesanan; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10) NOT NULL,
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    status character varying(50) NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: chat; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: info_barang_level; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_royalti real NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: item; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: kategori; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.kategori (
    nama character varying(255) NOT NULL,
    level integer NOT NULL,
    sub_dari character varying(255)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: kategori_item; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: pemesanan; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: pengembalian; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    nama_alamat_anggota character varying(255),
    no_ktp_anggota character varying(20)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: pengguna; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: pengiriman; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    nama_alamat_anggota character varying(255),
    no_ktp_anggota character varying(20)
);


-- ALTER .* OWNER TO db2018013;

--
-- Name: status; Type: TABLE; Schema: public; Owner: db2018013
--

CREATE TABLE public.status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


-- ALTER .* OWNER TO db2018013;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.admin (no_ktp) FROM stdin;
91
92
93
94
95
96
97
98
99
100
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
1	alamat_1	jalan_1	1	kota_1	15001
2	alamat_2	jalan_2	2	kota_2	15002
3	alamat_3	jalan_3	3	kota_3	15003
4	alamat_4	jalan_4	4	kota_4	15004
5	alamat_5	jalan_5	5	kota_5	15005
6	alamat_6	jalan_6	6	kota_6	15006
7	alamat_7	jalan_7	7	kota_7	15007
8	alamat_8	jalan_8	8	kota_8	15008
9	alamat_9	jalan_9	9	kota_9	15009
10	alamat_10	jalan_10	10	kota_10	15010
11	alamat_11	jalan_11	11	kota_11	15011
12	alamat_12	jalan_12	12	kota_12	15012
13	alamat_13	jalan_13	13	kota_13	15013
14	alamat_14	jalan_14	14	kota_14	15014
15	alamat_15	jalan_15	15	kota_15	15015
16	alamat_16	jalan_16	16	kota_16	15016
17	alamat_17	jalan_17	17	kota_17	15017
18	alamat_18	jalan_18	18	kota_18	15018
19	alamat_19	jalan_19	19	kota_19	15019
20	alamat_20	jalan_20	20	kota_20	15020
21	alamat_21	jalan_21	21	kota_21	15021
22	alamat_22	jalan_22	22	kota_22	15022
23	alamat_23	jalan_23	23	kota_23	15023
24	alamat_24	jalan_24	24	kota_24	15024
25	alamat_25	jalan_25	25	kota_25	15025
26	alamat_26	jalan_26	26	kota_26	15026
27	alamat_27	jalan_27	27	kota_27	15027
28	alamat_28	jalan_28	28	kota_28	15028
29	alamat_29	jalan_29	29	kota_29	15029
30	alamat_30	jalan_30	30	kota_30	15030
31	alamat_31	jalan_31	31	kota_31	15031
32	alamat_32	jalan_32	32	kota_32	15032
33	alamat_33	jalan_33	33	kota_33	15033
34	alamat_34	jalan_34	34	kota_34	15034
35	alamat_35	jalan_35	35	kota_35	15035
36	alamat_36	jalan_36	36	kota_36	15036
37	alamat_37	jalan_37	37	kota_37	15037
38	alamat_38	jalan_38	38	kota_38	15038
39	alamat_39	jalan_39	39	kota_39	15039
40	alamat_40	jalan_40	40	kota_40	15040
41	alamat_41	jalan_41	41	kota_41	15041
42	alamat_42	jalan_42	42	kota_42	15042
43	alamat_43	jalan_43	43	kota_43	15043
44	alamat_44	jalan_44	44	kota_44	15044
45	alamat_45	jalan_45	45	kota_45	15045
46	alamat_46	jalan_46	46	kota_46	15046
47	alamat_47	jalan_47	47	kota_47	15047
48	alamat_48	jalan_48	48	kota_48	15048
49	alamat_49	jalan_49	49	kota_49	15049
50	alamat_50	jalan_50	50	kota_50	15050
51	alamat_51	jalan_51	51	kota_51	15051
52	alamat_52	jalan_52	52	kota_52	15052
53	alamat_53	jalan_53	53	kota_53	15053
54	alamat_54	jalan_54	54	kota_54	15054
55	alamat_55	jalan_55	55	kota_55	15055
56	alamat_56	jalan_56	56	kota_56	15056
57	alamat_57	jalan_57	57	kota_57	15057
58	alamat_58	jalan_58	58	kota_58	15058
59	alamat_59	jalan_59	59	kota_59	15059
60	alamat_60	jalan_60	60	kota_60	15060
61	alamat_61	jalan_61	61	kota_61	15061
62	alamat_62	jalan_62	62	kota_62	15062
63	alamat_63	jalan_63	63	kota_63	15063
64	alamat_64	jalan_64	64	kota_64	15064
65	alamat_65	jalan_65	65	kota_65	15065
66	alamat_66	jalan_66	66	kota_66	15066
67	alamat_67	jalan_67	67	kota_67	15067
68	alamat_68	jalan_68	68	kota_68	15068
69	alamat_69	jalan_69	69	kota_69	15069
70	alamat_70	jalan_70	70	kota_70	15070
71	alamat_71	jalan_71	71	kota_71	15071
72	alamat_72	jalan_72	72	kota_72	15072
73	alamat_73	jalan_73	73	kota_73	15073
74	alamat_74	jalan_74	74	kota_74	15074
75	alamat_75	jalan_75	75	kota_75	15075
76	alamat_76	jalan_76	76	kota_76	15076
77	alamat_77	jalan_77	77	kota_77	15077
78	alamat_78	jalan_78	78	kota_78	15078
79	alamat_79	jalan_79	79	kota_79	15079
80	alamat_80	jalan_80	80	kota_80	15080
81	alamat_81	jalan_81	81	kota_81	15081
82	alamat_82	jalan_82	82	kota_82	15082
83	alamat_83	jalan_83	83	kota_83	15083
84	alamat_84	jalan_84	84	kota_84	15084
85	alamat_85	jalan_85	85	kota_85	15085
86	alamat_86	jalan_86	86	kota_86	15086
87	alamat_87	jalan_87	87	kota_87	15087
88	alamat_88	jalan_88	88	kota_88	15088
89	alamat_89	jalan_89	89	kota_89	15089
90	alamat_90	jalan_90	90	kota_90	15090
101	alamat_101	jalan_101	101	kota_101	15101
102	alamat_102	jalan_102	102	kota_102	15102
103	alamat_103	jalan_103	103	kota_103	15103
104	alamat_104	jalan_104	104	kota_104	15104
105	alamat_105	jalan_105	105	kota_105	15105
106	alamat_106	jalan_106	106	kota_106	15106
107	alamat_107	jalan_107	107	kota_107	15107
108	alamat_108	jalan_108	108	kota_108	15108
109	alamat_109	jalan_109	109	kota_109	15109
110	alamat_110	jalan_110	110	kota_110	15110
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.anggota (no_ktp, poin, level) FROM stdin;
1	5734	SILVER
3	5988	BRONZE
4	7093	SILVER
5	3177	GOLD
6	4422	SILVER
7	5958	SILVER
8	1586	PLATINUM
9	6824	BRONZE
10	3840	PLATINUM
11	8042	PLATINUM
12	7579	SILVER
13	1588	SILVER
14	8933	GOLD
15	7882	PLATINUM
16	6916	GOLD
17	6278	SILVER
18	8411	SILVER
19	9554	GOLD
20	4182	PLATINUM
21	7770	BRONZE
22	351	BRONZE
23	5272	BRONZE
24	4004	GOLD
25	4122	BRONZE
26	844	PLATINUM
27	8766	SILVER
28	784	SILVER
29	9641	BRONZE
30	9686	SILVER
31	7269	SILVER
32	6272	PLATINUM
33	5771	PLATINUM
34	1935	BRONZE
35	3614	SILVER
36	7247	SILVER
37	9111	BRONZE
38	6854	SILVER
39	230	BRONZE
40	4703	GOLD
41	4717	GOLD
42	7863	GOLD
43	8288	BRONZE
44	3322	PLATINUM
45	2021	GOLD
46	7244	GOLD
47	4069	BRONZE
48	3310	GOLD
49	9761	PLATINUM
50	54	PLATINUM
51	7311	BRONZE
52	9442	BRONZE
53	5929	PLATINUM
54	9826	PLATINUM
55	8408	SILVER
56	6647	SILVER
57	8802	GOLD
58	7213	GOLD
59	8108	GOLD
60	3944	BRONZE
61	6139	GOLD
62	644	SILVER
63	583	GOLD
64	8410	SILVER
65	6866	PLATINUM
66	4987	PLATINUM
67	1764	SILVER
68	5609	SILVER
69	7496	GOLD
70	6436	BRONZE
71	2256	PLATINUM
72	8567	GOLD
73	1184	SILVER
74	6847	SILVER
75	1836	BRONZE
76	566	GOLD
77	7144	SILVER
78	2931	GOLD
79	9801	PLATINUM
80	9588	PLATINUM
81	7721	PLATINUM
82	3026	PLATINUM
83	4450	PLATINUM
84	2369	GOLD
85	9008	BRONZE
86	7958	BRONZE
87	9999	GOLD
88	3837	GOLD
89	738	SILVER
90	3309	GOLD
101	8288	BRONZE
102	3322	PLATINUM
103	2021	GOLD
104	7244	GOLD
105	4069	BRONZE
106	3310	GOLD
107	9761	PLATINUM
108	54	PLATINUM
109	7311	BRONZE
110	9442	BRONZE
201	1000	PLATINUM
200	175	GOLD
2	4276	PLATINUM
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
BARANG_1	item_14	Pink	http://dummyimage.com/195x103.bmp/cc0000/ffffff	DALAM PERBAIKAN	4	9
BARANG_2	item_14	Aquamarine	http://dummyimage.com/133x150.bmp/ff4444/ffffff	SIAP	13	25
BARANG_3	item_5	Indigo	http://dummyimage.com/248x137.png/cc0000/ffffff	SIAP	1	53
BARANG_4	item_24	Aquamarine	http://dummyimage.com/169x170.png/dddddd/000000	SIAP	9	57
BARANG_5	item_2	Green	http://dummyimage.com/188x175.png/ff4444/ffffff	SIAP	6	62
BARANG_6	item_20	Orange	http://dummyimage.com/191x204.bmp/dddddd/000000	SIAP	22	6
BARANG_7	item_21	Green	http://dummyimage.com/172x148.bmp/dddddd/000000	SIAP	10	53
BARANG_8	item_19	Indigo	http://dummyimage.com/150x170.bmp/5fa2dd/ffffff	SIAP	16	53
BARANG_9	item_16	Mauv	http://dummyimage.com/193x156.bmp/ff4444/ffffff	SIAP	24	50
BARANG_10	item_15	Blue	http://dummyimage.com/114x193.jpg/dddddd/000000	SIAP	5	18
BARANG_11	item_1	Purple	http://dummyimage.com/212x212.bmp/5fa2dd/ffffff	SIAP	17	6
BARANG_12	item_8	Green	http://dummyimage.com/102x187.bmp/cc0000/ffffff	DALAM PERBAIKAN	6	1
BARANG_13	item_15	Teal	http://dummyimage.com/155x198.bmp/cc0000/ffffff	SEDANG DISEWA	17	24
BARANG_14	item_11	Crimson	http://dummyimage.com/203x188.bmp/cc0000/ffffff	SEDANG DISEWA	15	51
BARANG_15	item_19	Violet	http://dummyimage.com/202x188.bmp/cc0000/ffffff	DALAM PERBAIKAN	17	85
BARANG_16	item_4	Green	http://dummyimage.com/101x203.jpg/5fa2dd/ffffff	SIAP	16	72
BARANG_17	item_18	Red	http://dummyimage.com/106x193.bmp/cc0000/ffffff	SIAP	1	36
BARANG_18	item_9	Turquoise	http://dummyimage.com/135x189.bmp/5fa2dd/ffffff	SIAP	2	84
BARANG_19	item_9	Mauv	http://dummyimage.com/138x229.png/dddddd/000000	SIAP	10	4
BARANG_20	item_4	Crimson	http://dummyimage.com/195x221.jpg/dddddd/000000	SIAP	20	39
BARANG_21	item_21	Indigo	http://dummyimage.com/143x225.bmp/cc0000/ffffff	SIAP	13	17
BARANG_22	item_18	Purple	http://dummyimage.com/234x108.bmp/ff4444/ffffff	SEDANG DISEWA	12	48
BARANG_23	item_18	Aquamarine	http://dummyimage.com/186x194.png/dddddd/000000	SIAP	9	20
BARANG_24	item_5	Crimson	http://dummyimage.com/134x185.jpg/dddddd/000000	DALAM PERBAIKAN	24	7
BARANG_25	item_25	Crimson	http://dummyimage.com/233x140.jpg/cc0000/ffffff	DALAM PERBAIKAN	5	13
BARANG_26	item_22	Blue	http://dummyimage.com/246x231.jpg/5fa2dd/ffffff	SIAP	9	12
BARANG_27	item_9	Red	http://dummyimage.com/109x233.bmp/ff4444/ffffff	SEDANG DISEWA	18	3
BARANG_28	item_7	Indigo	http://dummyimage.com/200x137.bmp/cc0000/ffffff	SIAP	23	34
BARANG_29	item_21	Violet	http://dummyimage.com/185x140.jpg/dddddd/000000	SIAP	18	16
BARANG_30	item_9	Maroon	http://dummyimage.com/198x216.jpg/ff4444/ffffff	DALAM PERBAIKAN	7	20
BARANG_31	item_9	Red	http://dummyimage.com/124x118.bmp/5fa2dd/ffffff	SEDANG DISEWA	8	28
BARANG_32	item_12	Orange	http://dummyimage.com/130x167.jpg/5fa2dd/ffffff	DALAM PERBAIKAN	5	68
BARANG_33	item_19	Fuscia	http://dummyimage.com/157x195.png/dddddd/000000	SIAP	3	68
BARANG_34	item_16	Purple	http://dummyimage.com/247x182.png/5fa2dd/ffffff	SEDANG DISEWA	6	25
BARANG_35	item_23	Blue	http://dummyimage.com/121x101.bmp/5fa2dd/ffffff	DALAM PERBAIKAN	24	25
BARANG_36	item_20	Mauv	http://dummyimage.com/238x164.bmp/ff4444/ffffff	SIAP	2	45
BARANG_37	item_10	Green	http://dummyimage.com/121x111.bmp/cc0000/ffffff	SEDANG DISEWA	7	46
BARANG_38	item_10	Mauv	http://dummyimage.com/193x156.bmp/cc0000/ffffff	SIAP	4	27
BARANG_39	item_17	Red	http://dummyimage.com/147x197.png/dddddd/000000	SIAP	6	84
BARANG_40	item_23	Puce	http://dummyimage.com/156x239.png/ff4444/ffffff	SIAP	19	68
BARANG_41	item_24	Orange	http://dummyimage.com/233x129.bmp/5fa2dd/ffffff	SEDANG DISEWA	22	1
BARANG_43	item_15	Teal	http://dummyimage.com/159x108.bmp/5fa2dd/ffffff	SIAP	15	76
BARANG_44	item_12	Yellow	http://dummyimage.com/193x137.png/5fa2dd/ffffff	SEDANG DISEWA	10	65
BARANG_45	item_14	Mauv	http://dummyimage.com/104x113.bmp/cc0000/ffffff	SIAP	9	66
BARANG_46	item_22	Puce	http://dummyimage.com/248x174.png/dddddd/000000	DALAM PERBAIKAN	18	75
BARANG_47	item_1	Purple	http://dummyimage.com/149x174.png/ff4444/ffffff	SIAP	7	73
BARANG_48	item_13	Pink	http://dummyimage.com/154x149.jpg/5fa2dd/ffffff	SIAP	23	16
BARANG_49	item_13	Purple	http://dummyimage.com/174x145.bmp/5fa2dd/ffffff	DALAM PERBAIKAN	1	38
BARANG_50	item_5	Maroon	http://dummyimage.com/206x157.bmp/cc0000/ffffff	DALAM PERBAIKAN	9	10
BARANG_51	item_6	Khaki	http://dummyimage.com/191x239.png/ff4444/ffffff	DALAM PERBAIKAN	2	10
BARANG_52	item_15	Khaki	http://dummyimage.com/185x224.png/cc0000/ffffff	SIAP	6	52
BARANG_53	item_1	Khaki	http://dummyimage.com/200x126.jpg/dddddd/000000	SIAP	22	31
BARANG_54	item_21	Orange	http://dummyimage.com/122x114.png/ff4444/ffffff	SEDANG DISEWA	14	11
BARANG_55	item_1	Crimson	http://dummyimage.com/139x189.png/5fa2dd/ffffff	SIAP	22	69
BARANG_56	item_20	Khaki	http://dummyimage.com/102x129.bmp/cc0000/ffffff	DALAM PERBAIKAN	5	4
BARANG_57	item_8	Goldenrod	http://dummyimage.com/228x243.bmp/5fa2dd/ffffff	SEDANG DISEWA	6	40
BARANG_58	item_23	Blue	http://dummyimage.com/250x137.bmp/cc0000/ffffff	DALAM PERBAIKAN	21	21
BARANG_59	item_7	Indigo	http://dummyimage.com/168x150.png/dddddd/000000	SIAP	23	47
BARANG_60	item_17	Green	http://dummyimage.com/215x244.jpg/5fa2dd/ffffff	DALAM PERBAIKAN	6	25
BARANG_61	item_12	Crimson	http://dummyimage.com/125x203.png/cc0000/ffffff	SIAP	6	66
BARANG_62	item_9	Maroon	http://dummyimage.com/112x200.png/cc0000/ffffff	DALAM PERBAIKAN	14	45
BARANG_63	item_10	Red	http://dummyimage.com/194x137.jpg/ff4444/ffffff	SEDANG DISEWA	6	50
BARANG_64	item_13	Mauv	http://dummyimage.com/151x199.jpg/ff4444/ffffff	SEDANG DISEWA	12	39
BARANG_65	item_12	Violet	http://dummyimage.com/147x166.bmp/ff4444/ffffff	SIAP	7	42
BARANG_66	item_15	Fuscia	http://dummyimage.com/218x228.png/dddddd/000000	SIAP	10	4
BARANG_67	item_25	Pink	http://dummyimage.com/143x183.jpg/cc0000/ffffff	SIAP	24	66
BARANG_68	item_10	Turquoise	http://dummyimage.com/189x172.jpg/ff4444/ffffff	SEDANG DISEWA	12	77
BARANG_69	item_13	Goldenrod	http://dummyimage.com/102x131.bmp/5fa2dd/ffffff	SIAP	23	86
BARANG_70	item_16	Teal	http://dummyimage.com/123x125.bmp/dddddd/000000	SIAP	16	72
BARANG_71	item_7	Crimson	http://dummyimage.com/171x215.bmp/cc0000/ffffff	SIAP	4	32
BARANG_72	item_5	Purple	http://dummyimage.com/160x236.png/5fa2dd/ffffff	SIAP	8	38
BARANG_73	item_24	Pink	http://dummyimage.com/209x207.jpg/5fa2dd/ffffff	DALAM PERBAIKAN	6	53
BARANG_74	item_6	Khaki	http://dummyimage.com/182x151.jpg/cc0000/ffffff	SIAP	13	81
BARANG_75	item_14	Yellow	http://dummyimage.com/211x173.png/5fa2dd/ffffff	SIAP	23	68
BARANG_76	item_8	Khaki	http://dummyimage.com/226x194.png/ff4444/ffffff	SEDANG DISEWA	11	4
BARANG_77	item_1	Fuscia	http://dummyimage.com/141x139.bmp/cc0000/ffffff	SIAP	22	81
BARANG_78	item_19	Maroon	http://dummyimage.com/249x170.bmp/ff4444/ffffff	SIAP	23	71
BARANG_79	item_9	Yellow	http://dummyimage.com/219x145.png/5fa2dd/ffffff	SEDANG DISEWA	19	28
BARANG_80	item_11	Yellow	http://dummyimage.com/195x203.png/dddddd/000000	DALAM PERBAIKAN	7	55
BARANG_81	item_10	Aquamarine	http://dummyimage.com/161x194.bmp/5fa2dd/ffffff	DALAM PERBAIKAN	14	86
BARANG_82	item_16	Fuscia	http://dummyimage.com/104x167.bmp/cc0000/ffffff	SIAP	15	22
BARANG_83	item_16	Mauv	http://dummyimage.com/222x158.jpg/5fa2dd/ffffff	SEDANG DISEWA	23	57
BARANG_84	item_21	Fuscia	http://dummyimage.com/169x118.jpg/5fa2dd/ffffff	DALAM PERBAIKAN	5	34
BARANG_85	item_13	Pink	http://dummyimage.com/205x225.jpg/5fa2dd/ffffff	SIAP	5	78
BARANG_86	item_24	\N	http://dummyimage.com/176x176.png/5fa2dd/ffffff	DALAM PERBAIKAN	23	5
BARANG_87	item_16	Indigo	http://dummyimage.com/129x203.bmp/cc0000/ffffff	SIAP	22	66
BARANG_88	item_20	Purple	http://dummyimage.com/203x224.bmp/ff4444/ffffff	SIAP	8	70
BARANG_89	item_12	Maroon	http://dummyimage.com/106x196.bmp/5fa2dd/ffffff	SEDANG DISEWA	22	72
BARANG_90	item_8	Violet	http://dummyimage.com/238x244.jpg/cc0000/ffffff	SEDANG DISEWA	6	41
BARANG_91	item_11	Fuscia	http://dummyimage.com/188x115.png/ff4444/ffffff	DALAM PERBAIKAN	21	48
BARANG_92	item_18	Aquamarine	http://dummyimage.com/194x126.jpg/ff4444/ffffff	SEDANG DISEWA	7	28
BARANG_93	item_2	Pink	http://dummyimage.com/216x174.png/dddddd/000000	DALAM PERBAIKAN	8	68
BARANG_94	item_14	Fuscia	http://dummyimage.com/249x125.jpg/ff4444/ffffff	SIAP	1	55
BARANG_95	item_22	Teal	http://dummyimage.com/153x162.jpg/cc0000/ffffff	SIAP	15	73
BARANG_96	item_5	Crimson	http://dummyimage.com/151x132.png/ff4444/ffffff	DALAM PERBAIKAN	21	31
BARANG_97	item_24	Violet	http://dummyimage.com/242x150.jpg/cc0000/ffffff	SEDANG DISEWA	10	17
BARANG_98	item_24	Puce	http://dummyimage.com/100x175.png/5fa2dd/ffffff	SEDANG DISEWA	4	46
BARANG_99	item_9	Puce	http://dummyimage.com/249x182.jpg/cc0000/ffffff	DALAM PERBAIKAN	15	31
BARANG_100	item_18	Green	http://dummyimage.com/172x250.bmp/ff4444/ffffff	SIAP	18	6
BARANG_42	item_2	Blue	http://dummyimage.com/163x111.jpg/cc0000/ffffff	sedang disewa anggota	21	49
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.barang_dikembalikan (no_resi, no_urut, id_barang) FROM stdin;
1001100002	URUTAN_1	BARANG_2
1001100002	URUTAN_2	BARANG_26
1001100003	URUTAN_3	BARANG_43
1001100003	URUTAN_4	BARANG_8
1001100004	URUTAN_5	BARANG_44
1001100004	URUTAN_6	BARANG_35
1001100005	URUTAN_7	BARANG_60
1001100005	URUTAN_8	BARANG_36
1001100006	URUTAN_9	BARANG_6
1001100006	URUTAN_11	BARANG_76
1001100007	URUTAN_12	BARANG_54
1001100007	URUTAN_13	BARANG_71
1001100008	URUTAN_14	BARANG_87
1001100008	URUTAN_15	BARANG_90
1001100009	URUTAN_16	BARANG_86
1001100009	URUTAN_17	BARANG_46
1001100010	URUTAN_18	BARANG_81
1001100010	URUTAN_19	BARANG_23
1001100011	URUTAN_20	BARANG_45
1001100011	URUTAN_21	BARANG_7
1001100012	URUTAN_23	BARANG_100
1001100012	URUTAN_29	BARANG_40
1001100015	URUTAN_32	BARANG_23
1001100015	URUTAN_37	BARANG_68
1001100016	URUTAN_38	BARANG_66
1001100016	URUTAN_40	BARANG_54
1001100017	URUTAN_41	BARANG_5
1001100017	URUTAN_48	BARANG_32
1001100020	URUTAN_59	BARANG_60
1001100020	URUTAN_60	BARANG_97
1001100021	URUTAN_62	BARANG_42
1001100021	URUTAN_67	BARANG_7
1001100022	URUTAN_79	BARANG_58
1001100022	URUTAN_82	BARANG_97
1001100023	URUTAN_86	BARANG_13
1001100023	URUTAN_87	BARANG_16
1001100026	URUTAN_88	BARANG_88
1001100026	URUTAN_95	BARANG_51
1001100031	URUTAN_96	BARANG_2
1001100031	URUTAN_97	BARANG_60
1001100036	URUTAN_98	BARANG_86
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
1001100002	URUTAN_1	BARANG_2	2017-04-24	Bagus barangnya.
1001100002	URUTAN_2	BARANG_26	2017-04-29	Cepat sampai, tidak ribet packagingnya, kualitas barang bintang lima. Keren!
1001100003	URUTAN_3	BARANG_43	2017-05-06	Kurang bagus packagingnya.
1001100003	URUTAN_4	BARANG_8	2017-05-07	Baik sekali kualitasnya.
1001100004	URUTAN_5	BARANG_44	2017-05-10	Bagus barangnya.
1001100004	URUTAN_6	BARANG_35	2017-05-26	Tidak telat. Bagus.
1001100005	URUTAN_7	BARANG_60	2017-06-13	Mungkin bisa diberikan packaging yang lebih layak dibandingkan hanya bungkus kertas.
1001100005	URUTAN_8	BARANG_36	2017-06-21	Tidak ada yang kurang dari pelayanannya. Mantap!
1001100006	URUTAN_9	BARANG_6	2017-06-23	Cepat sampai, tidak ribet packagingnya, kualitas barang bintang lima. Keren!
1001100006	URUTAN_10	BARANG_43	2017-07-18	Barang datang tepat waktu. Kualitas baik.
1001100007	URUTAN_11	BARANG_76	2017-07-30	Barang sedikit bernoda dan tidak bisa dibersihkan, tapi tidak begitu mencolok.
1001100007	URUTAN_12	BARANG_54	2017-08-09	Sudah baik packagingnya,
1001100008	URUTAN_13	BARANG_71	2017-08-14	Sudah baik packagingnya,
1001100008	URUTAN_14	BARANG_87	2017-09-03	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100009	URUTAN_15	BARANG_90	2017-09-05	Bagus barangnya.
1001100009	URUTAN_16	BARANG_86	2017-09-13	Tidak telat. Bagus.
1001100010	URUTAN_17	BARANG_46	2017-09-22	Barang datang tepat waktu. Kualitas baik.
1001100010	URUTAN_18	BARANG_81	2017-09-25	Tidak telat. Bagus.
1001100011	URUTAN_19	BARANG_23	2017-10-15	Barang datang tepat waktu. Kualitas baik.
1001100011	URUTAN_20	BARANG_45	2017-10-18	Mungkin bisa diberikan packaging yang lebih layak dibandingkan hanya bungkus kertas.
1001100012	URUTAN_21	BARANG_7	2017-10-20	Bagus barangnya.
1001100012	URUTAN_23	BARANG_100	2017-10-27	Tidak telat. Bagus.
1001100015	URUTAN_27	BARANG_54	2017-11-30	Barang datang tepat waktu. Kualitas baik.
1001100015	URUTAN_29	BARANG_40	2017-12-11	Tidak telat. Bagus.
1001100016	URUTAN_31	BARANG_15	2017-12-30	Bagus barangnya.
1001100016	URUTAN_32	BARANG_23	2018-01-21	Barang datang tepat waktu. Kualitas baik.
1001100017	URUTAN_33	BARANG_78	2018-01-23	Bagus barangnya.
1001100017	URUTAN_35	BARANG_73	2018-02-02	Barang datang tepat waktu. Kualitas baik.
1001100018	URUTAN_37	BARANG_68	2018-02-24	Saya suka dengan kualitas barang yang ditawarkan. Mantap!
1001100018	URUTAN_38	BARANG_66	2018-02-27	Barang datang tepat waktu. Kualitas baik.
1001100020	URUTAN_40	BARANG_54	2018-03-04	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100020	URUTAN_41	BARANG_5	2018-03-10	Tidak telat. Bagus.
1001100021	URUTAN_45	BARANG_85	2018-04-03	Bagus barangnya.
1001100021	URUTAN_48	BARANG_32	2018-04-13	Tidak telat. Bagus.
1001100022	URUTAN_55	BARANG_33	2018-05-20	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100022	URUTAN_57	BARANG_76	2018-05-29	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100023	URUTAN_59	BARANG_60	2018-06-21	Mungkin bisa diberikan packaging yang lebih layak dibandingkan hanya bungkus kertas.
1001100023	URUTAN_60	BARANG_97	2018-06-22	Tidak ada yang kurang dari pelayanannya. Mantap!
1001100024	URUTAN_62	BARANG_42	2018-08-14	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100024	URUTAN_66	BARANG_84	2018-09-19	Mungkin bisa diberikan packaging yang lebih layak dibandingkan hanya bungkus kertas.
1001100025	URUTAN_67	BARANG_7	2018-09-28	Tidak ada yang kurang dari pelayanannya. Mantap!
1001100025	URUTAN_69	BARANG_60	2018-10-05	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100026	URUTAN_79	BARANG_58	2018-11-18	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100026	URUTAN_82	BARANG_97	2018-12-04	sudah_dikembalikan
1001100028	URUTAN_86	BARANG_13	2018-12-26	Kurang bagus packagingnya.
1001100028	URUTAN_87	BARANG_16	2018-12-30	Baik sekali kualitasnya.
1001100029	URUTAN_88	BARANG_88	2019-01-05	Barang datang tepat waktu. Kualitas baik.
1001100029	URUTAN_89	BARANG_89	2019-01-07	Wah mantap nih datangnya tepat waktu, barangnya mulus.
1001100031	URUTAN_95	BARANG_51	2019-02-17	Mungkin bisa diberikan packaging yang lebih layak dibandingkan hanya bungkus kertas.
1001100031	URUTAN_96	BARANG_2	2019-02-20	Tidak ada yang kurang dari pelayanannya. Mantap!
1001100032	URUTAN_97	BARANG_60	2019-03-23	Baik sekali kualitasnya.
1001100032	URUTAN_98	BARANG_86	2019-03-24	Wah mantap nih datangnya tepat waktu, barangnya mulus.
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, status) FROM stdin;
order_1	URUTAN_1	BARANG_60	2017-06-13	11	2018-05-13	dalam_masa_sewa
order_2	URUTAN_2	BARANG_76	2017-07-30	7	2018-02-28	sudah_dikembalikan
order_3	URUTAN_3	BARANG_51	2019-02-17	20	2020-10-17	sudah_dikembalikan
order_4	URUTAN_4	BARANG_24	2018-10-28	10	2019-08-28	batal
order_5	URUTAN_5	BARANG_80	2018-04-21	9	2019-01-21	batal
order_6	URUTAN_6	BARANG_73	2018-02-24	19	2019-09-24	batal
order_7	URUTAN_7	BARANG_89	2019-01-07	19	2020-08-07	dalam_masa_sewa
order_8	URUTAN_8	BARANG_23	2018-01-21	3	2018-04-21	dalam_masa_sewa
order_9	URUTAN_9	BARANG_60	2019-03-23	20	2020-11-23	sudah_dikembalikan
order_10	URUTAN_10	BARANG_13	2018-09-19	12	2019-09-19	sedang_disiapkan
order_11	URUTAN_11	BARANG_82	2018-11-06	1	2018-12-06	batal
order_12	URUTAN_12	BARANG_86	2019-03-24	16	2020-07-24	sudah_dikembalikan
order_13	URUTAN_13	BARANG_55	2017-11-30	9	2018-08-30	batal
order_14	URUTAN_14	BARANG_71	2019-02-14	13	2020-03-14	batal
order_15	URUTAN_15	BARANG_24	2018-11-11	2	2019-01-11	batal
order_16	URUTAN_16	BARANG_43	2017-05-06	7	2017-12-06	dalam_masa_sewa
order_17	URUTAN_17	BARANG_97	2018-12-04	8	2019-08-04	sudah_dikembalikan
order_18	URUTAN_18	BARANG_32	2018-04-13	16	2019-08-13	sudah_dikembalikan
order_19	URUTAN_19	BARANG_81	2018-03-30	4	2018-07-30	batal
order_20	URUTAN_20	BARANG_58	2018-11-18	6	2019-05-18	sudah_dikembalikan
order_21	URUTAN_21	BARANG_71	2017-08-14	12	2018-08-14	batal
order_22	URUTAN_22	BARANG_90	2017-09-05	9	2018-06-05	sudah_dikembalikan
order_23	URUTAN_23	BARANG_75	2018-05-18	10	2019-03-18	batal
order_24	URUTAN_24	BARANG_97	2018-06-22	10	2019-04-22	sudah_dikembalikan
order_25	URUTAN_25	BARANG_26	2018-04-12	12	2019-04-12	batal
order_26	URUTAN_26	BARANG_60	2018-06-21	21	2020-03-21	sudah_dikembalikan
order_27	URUTAN_27	BARANG_33	2018-05-20	11	2019-04-20	dalam_masa_sewa
order_28	URUTAN_28	BARANG_49	2018-05-20	6	2018-11-20	batal
order_29	URUTAN_29	BARANG_77	2018-04-21	2	2018-06-21	batal
order_30	URUTAN_30	BARANG_60	2018-10-05	22	2020-08-05	dalam_masa_sewa
order_31	URUTAN_31	BARANG_54	2018-03-04	10	2019-01-04	dalam_masa_sewa
order_32	URUTAN_32	BARANG_53	2018-12-22	6	2019-06-22	batal
order_33	URUTAN_33	BARANG_58	2018-04-12	19	2019-11-12	batal
order_34	URUTAN_34	BARANG_81	2018-11-05	2	2019-01-05	batal
order_35	URUTAN_35	BARANG_12	2018-12-15	13	2020-01-15	batal
order_36	URUTAN_36	BARANG_13	2018-12-26	8	2019-08-26	sudah_dikembalikan
order_37	URUTAN_37	BARANG_81	2017-09-25	16	2019-01-25	sedang_dikirim
order_38	URUTAN_38	BARANG_35	2017-05-26	9	2018-02-26	sedang_dikonfirmasi
order_39	URUTAN_39	BARANG_22	2018-11-01	3	2019-02-01	batal
order_40	URUTAN_40	BARANG_7	2018-09-28	23	2020-08-28	sudah_dikembalikan
order_41	URUTAN_41	BARANG_81	2018-10-26	13	2019-11-26	batal
order_42	URUTAN_42	BARANG_2	2017-04-24	11	2018-03-24	sedang_disiapkan
order_43	URUTAN_43	BARANG_15	2019-01-31	3	2019-04-30	sedang_disiapkan
order_44	URUTAN_44	BARANG_16	2018-12-30	10	2019-10-30	sudah_dikembalikan
order_45	URUTAN_45	BARANG_30	2018-11-04	24	2020-11-04	batal
order_46	URUTAN_46	BARANG_67	2018-06-12	12	2019-06-12	batal
order_47	URUTAN_47	BARANG_44	2017-05-10	10	2018-03-10	batal
order_48	URUTAN_48	BARANG_66	2018-02-27	5	2018-07-27	sedang_disiapkan
order_49	URUTAN_49	BARANG_20	2018-11-25	16	2020-03-25	sedang_dikonfirmasi
order_50	URUTAN_50	BARANG_43	2017-07-18	23	2019-06-18	sudah_dikembalikan
order_1	URUTAN_51	BARANG_36	2017-06-21	2	2017-08-21	sudah_dikembalikan
order_2	URUTAN_52	BARANG_83	2018-06-30	16	2019-10-30	batal
order_3	URUTAN_53	BARANG_73	2018-02-02	20	2019-10-02	dalam_masa_sewa
order_4	URUTAN_54	BARANG_85	2018-04-03	18	2019-10-03	sedang_dikirim
order_5	URUTAN_55	BARANG_65	2018-03-02	15	2019-06-02	batal
order_6	URUTAN_56	BARANG_76	2018-05-29	12	2019-05-29	sedang_dikirim
order_7	URUTAN_57	BARANG_40	2017-12-11	9	2018-09-11	dalam_masa_sewa
order_8	URUTAN_58	BARANG_15	2017-12-30	21	2019-09-30	dalam_masa_sewa
order_9	URUTAN_59	BARANG_73	2019-01-29	21	2020-10-29	batal
order_10	URUTAN_60	BARANG_51	2017-11-10	17	2019-04-10	batal
order_11	URUTAN_61	BARANG_73	2017-10-24	16	2019-02-24	batal
order_12	URUTAN_62	BARANG_68	2018-02-24	6	2018-08-24	batal
order_13	URUTAN_63	BARANG_100	2017-10-27	20	2019-06-27	sudah_dikembalikan
order_14	URUTAN_64	BARANG_2	2019-02-20	13	2020-03-20	sudah_dikembalikan
order_15	URUTAN_65	BARANG_16	2018-08-17	23	2020-07-17	batal
order_16	URUTAN_66	BARANG_59	2017-12-16	15	2019-03-16	batal
order_17	URUTAN_67	BARANG_20	2018-10-02	16	2020-02-02	batal
order_18	URUTAN_68	BARANG_80	2018-12-03	24	2020-12-03	batal
order_19	URUTAN_69	BARANG_45	2017-10-18	4	2018-02-18	batal
order_20	URUTAN_70	BARANG_99	2018-08-31	22	2020-06-30	batal
order_21	URUTAN_71	BARANG_42	2018-08-14	6	2019-02-14	dalam_masa_sewa
order_22	URUTAN_72	BARANG_62	2018-03-19	8	2018-11-19	batal
order_23	URUTAN_73	BARANG_72	2018-10-28	10	2019-08-28	sedang_disiapkan
order_24	URUTAN_74	BARANG_24	2018-04-17	23	2020-03-17	batal
order_25	URUTAN_75	BARANG_7	2017-10-20	14	2018-12-20	sudah_dikembalikan
order_26	URUTAN_76	BARANG_46	2017-09-22	2	2017-11-22	sudah_dikembalikan
order_27	URUTAN_77	BARANG_51	2017-12-09	6	2018-06-09	batal
order_28	URUTAN_78	BARANG_6	2017-06-23	16	2018-10-23	batal
order_29	URUTAN_79	BARANG_31	2018-10-21	7	2019-05-21	batal
order_30	URUTAN_80	BARANG_16	2018-01-29	3	2018-04-29	batal
order_31	URUTAN_81	BARANG_8	2017-05-07	8	2018-01-07	dalam_masa_sewa
order_32	URUTAN_82	BARANG_26	2017-04-29	11	2018-03-29	batal
order_33	URUTAN_83	BARANG_23	2017-10-15	24	2019-10-15	batal
order_34	URUTAN_84	BARANG_54	2017-08-09	8	2018-04-09	sedang_dikirim
order_35	URUTAN_85	BARANG_85	2018-05-07	19	2019-12-07	batal
order_36	URUTAN_86	BARANG_91	2018-03-24	2	2018-05-24	batal
order_37	URUTAN_87	BARANG_88	2019-01-05	7	2019-08-05	sudah_dikembalikan
order_38	URUTAN_88	BARANG_86	2017-09-13	1	2017-10-13	dalam_masa_sewa
order_39	URUTAN_89	BARANG_46	2019-01-22	4	2019-05-22	batal
order_40	URUTAN_90	BARANG_54	2017-11-30	24	2019-11-30	dalam_masa_sewa
order_41	URUTAN_91	BARANG_18	2018-04-14	14	2019-06-14	batal
order_42	URUTAN_92	BARANG_84	2018-09-19	15	2019-12-19	dalam_masa_sewa
order_43	URUTAN_93	BARANG_5	2018-03-10	8	2018-11-10	sedang_dikonfirmasi
order_44	URUTAN_94	BARANG_87	2017-09-03	18	2019-03-03	dalam_masa_sewa
order_45	URUTAN_95	BARANG_88	2018-12-25	14	2020-02-25	batal
order_46	URUTAN_96	BARANG_47	2019-04-03	18	2020-10-03	batal
order_47	URUTAN_97	BARANG_27	2019-03-26	24	2021-03-26	batal
order_48	URUTAN_98	BARANG_59	2019-02-15	24	2021-02-15	batal
order_49	URUTAN_99	BARANG_78	2018-01-23	20	2019-09-23	dalam_masa_sewa
order_50	URUTAN_100	BARANG_99	2017-11-17	22	2019-09-17	batal
order_1	URUTAN_200	BARANG_42	2017-06-13	1	2017-07-13	batal
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.chat (id, pesan, date_time, no_ktp_anggota, no_ktp_admin) FROM stdin;
1	lorem ipsum pesan_1	2019-02-22 06:24:05	1	91
2	lorem ipsum pesan_2	2019-02-23 11:02:23	2	92
3	lorem ipsum pesan_3	2018-09-13 05:14:56	3	93
4	lorem ipsum pesan_4	2018-08-17 06:41:17	4	94
5	lorem ipsum pesan_5	2018-11-19 23:11:03	5	95
6	lorem ipsum pesan_6	2018-05-04 06:41:36	6	96
7	lorem ipsum pesan_7	2019-02-02 23:45:26	7	97
8	lorem ipsum pesan_8	2019-02-13 22:56:04	8	98
9	lorem ipsum pesan_9	2019-02-26 06:38:54	9	99
10	lorem ipsum pesan_10	2018-07-29 18:13:00	10	100
11	lorem ipsum pesan_11	2018-09-12 06:37:02	11	91
12	lorem ipsum pesan_12	2018-12-29 15:14:57	12	92
13	lorem ipsum pesan_13	2018-10-24 10:29:27	13	93
14	lorem ipsum pesan_14	2019-04-08 14:27:53	14	94
15	lorem ipsum pesan_15	2018-05-23 11:54:06	15	95
16	lorem ipsum pesan_16	2018-09-30 01:57:43	16	96
17	lorem ipsum pesan_17	2018-07-23 19:42:57	17	97
18	lorem ipsum pesan_18	2018-09-15 09:36:36	18	98
19	lorem ipsum pesan_19	2019-03-30 05:33:50	19	99
20	lorem ipsum pesan_20	2018-12-21 13:34:24	20	100
21	lorem ipsum pesan_21	2018-09-21 09:11:10	21	91
22	lorem ipsum pesan_22	2018-12-20 17:25:59	22	92
23	lorem ipsum pesan_23	2018-06-13 04:53:32	23	93
24	lorem ipsum pesan_24	2019-04-04 11:14:42	24	94
25	lorem ipsum pesan_25	2018-12-20 11:20:32	25	95
26	lorem ipsum pesan_26	2018-07-04 21:43:35	26	96
27	lorem ipsum pesan_27	2018-08-30 02:24:37	27	97
28	lorem ipsum pesan_28	2018-12-10 02:42:35	28	98
29	lorem ipsum pesan_29	2019-03-29 15:59:22	29	99
30	lorem ipsum pesan_30	2018-10-09 03:11:04	30	100
31	lorem ipsum pesan_31	2018-10-14 06:29:59	31	91
32	lorem ipsum pesan_32	2018-12-08 18:32:36	32	92
33	lorem ipsum pesan_33	2019-03-08 03:53:01	33	93
34	lorem ipsum pesan_34	2018-04-21 17:06:02	34	94
35	lorem ipsum pesan_35	2019-04-07 11:31:35	35	95
36	lorem ipsum pesan_36	2018-06-25 15:54:17	36	96
37	lorem ipsum pesan_37	2018-08-18 19:57:34	37	97
38	lorem ipsum pesan_38	2018-12-09 21:11:50	38	98
39	lorem ipsum pesan_39	2019-01-19 05:52:57	39	99
40	lorem ipsum pesan_40	2018-04-22 00:24:39	40	100
41	lorem ipsum pesan_41	2018-08-27 18:32:46	41	91
42	lorem ipsum pesan_42	2018-12-20 18:15:50	42	92
43	lorem ipsum pesan_43	2018-11-29 08:53:04	43	93
44	lorem ipsum pesan_44	2019-01-28 23:48:51	44	94
45	lorem ipsum pesan_45	2019-01-23 06:06:41	45	95
46	lorem ipsum pesan_46	2018-05-21 04:50:46	46	96
47	lorem ipsum pesan_47	2018-10-28 09:38:29	47	97
48	lorem ipsum pesan_48	2019-03-08 17:38:18	48	98
49	lorem ipsum pesan_49	2019-04-04 09:16:00	49	99
50	lorem ipsum pesan_50	2018-07-25 08:34:06	50	100
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
BARANG_77	PLATINUM	3623840	0.370000005
BARANG_33	BRONZE	902991	0.889999986
BARANG_27	GOLD	1941297.88	0.800000012
BARANG_43	GOLD	7523151	0.469999999
BARANG_53	BRONZE	4998591	0.340000004
BARANG_54	PLATINUM	6653973.5	0.230000004
BARANG_86	SILVER	1256078.75	0.939999998
BARANG_69	BRONZE	1099610.88	0.280000001
BARANG_58	PLATINUM	9397989	0.0199999996
BARANG_47	GOLD	2936089.75	0.0199999996
BARANG_64	PLATINUM	8222127.5	0.150000006
BARANG_65	BRONZE	2822743.5	0.959999979
BARANG_90	SILVER	5803338	0.5
BARANG_42	SILVER	255731.078	0.280000001
BARANG_25	GOLD	6915914	0.479999989
BARANG_52	PLATINUM	1089010.88	0.810000002
BARANG_85	PLATINUM	6138325.5	0.439999998
BARANG_75	PLATINUM	399901.656	0.25
BARANG_64	BRONZE	6126633.5	0.419999987
BARANG_68	BRONZE	2809920	0.319999993
BARANG_8	SILVER	6221391	0.129999995
BARANG_17	PLATINUM	4134056	0.550000012
BARANG_63	BRONZE	3826503.25	0.469999999
BARANG_90	BRONZE	2349963.5	0.889999986
BARANG_93	BRONZE	4799730.5	0.150000006
BARANG_67	BRONZE	3223676.25	0.639999986
BARANG_50	GOLD	16977.4609	0.75999999
BARANG_85	GOLD	2653678.25	0.790000021
BARANG_60	SILVER	5227022	0.479999989
BARANG_14	GOLD	1583360	0.569999993
BARANG_39	GOLD	5840212	0.349999994
BARANG_9	PLATINUM	9327502	0.550000012
BARANG_72	BRONZE	379214.375	0.699999988
BARANG_32	PLATINUM	4873471	0.50999999
BARANG_37	SILVER	7800130	0.839999974
BARANG_21	BRONZE	8752476	0.280000001
BARANG_29	BRONZE	6487284	0.610000014
BARANG_94	GOLD	3279153	0.529999971
BARANG_6	SILVER	1110337	0.639999986
BARANG_23	BRONZE	8314794.5	0.200000003
BARANG_13	SILVER	7207111	0.670000017
BARANG_97	GOLD	1472286	0.400000006
BARANG_59	SILVER	8482578	0.239999995
BARANG_76	SILVER	1458928.62	0.0799999982
BARANG_43	PLATINUM	4804728	0.119999997
BARANG_82	BRONZE	814158.125	0.680000007
BARANG_41	SILVER	9234578	0.00999999978
BARANG_54	SILVER	7493724	0.400000006
BARANG_41	PLATINUM	7029836	0.0500000007
BARANG_21	PLATINUM	1454213	0.720000029
BARANG_80	BRONZE	4583129.5	0.119999997
BARANG_55	BRONZE	1032063.5	0.680000007
BARANG_40	BRONZE	2374954.25	0.479999989
BARANG_38	PLATINUM	9177002	0.970000029
BARANG_94	SILVER	3293975	0.230000004
BARANG_1	BRONZE	5300058	0.519999981
BARANG_84	GOLD	9217612	0.680000007
BARANG_39	BRONZE	230555.312	0.119999997
BARANG_86	PLATINUM	2854979.25	0.0299999993
BARANG_26	PLATINUM	4221673	0.0700000003
BARANG_68	PLATINUM	2178624	0.0900000036
BARANG_2	SILVER	1186029.62	0.409999996
BARANG_31	GOLD	4823631	0.100000001
BARANG_98	GOLD	19464.6406	0.50999999
BARANG_69	PLATINUM	6439908	0.0599999987
BARANG_97	SILVER	8898531	0.200000003
BARANG_82	GOLD	8524779	0.949999988
BARANG_84	BRONZE	1903361.5	0.810000002
BARANG_40	GOLD	1336961.25	0.0299999993
BARANG_59	GOLD	1043759.38	0.109999999
BARANG_29	SILVER	9077941	0.289999992
BARANG_66	GOLD	144870.562	0.939999998
BARANG_98	BRONZE	8679897	0.75
BARANG_24	GOLD	531695.938	0.779999971
BARANG_52	GOLD	2066533	0.720000029
BARANG_78	BRONZE	7358395	0.389999986
BARANG_6	BRONZE	5134955	0.970000029
BARANG_4	GOLD	5234051	0.0599999987
BARANG_95	GOLD	1102698	0.870000005
BARANG_21	SILVER	6448857.5	0.200000003
BARANG_10	BRONZE	6090941	0.0700000003
BARANG_75	BRONZE	9880236	0.769999981
BARANG_65	SILVER	1361299.12	0.100000001
BARANG_45	PLATINUM	4827622	0.230000004
BARANG_81	SILVER	9054112	0.00999999978
BARANG_53	SILVER	9426012	0.800000012
BARANG_1	PLATINUM	7448333	0.810000002
BARANG_66	PLATINUM	8026746.5	0.300000012
BARANG_32	SILVER	8066482	0.829999983
BARANG_61	PLATINUM	1735326.88	0.689999998
BARANG_12	PLATINUM	3482089.75	0.819999993
BARANG_6	PLATINUM	7380235	0.239999995
BARANG_36	GOLD	2417372.75	0.460000008
BARANG_57	GOLD	9895812	0.75999999
BARANG_41	GOLD	6432702	0.150000006
BARANG_23	SILVER	1862809	0.170000002
BARANG_86	BRONZE	1632648	0.980000019
BARANG_74	BRONZE	6584951	0.949999988
BARANG_87	PLATINUM	9935093	0.860000014
BARANG_36	BRONZE	5358105.5	0.319999993
BARANG_69	GOLD	3921482	0.74000001
BARANG_64	GOLD	4297415	0.0700000003
BARANG_56	GOLD	1870661.5	0.409999996
BARANG_73	PLATINUM	698212.562	0.870000005
BARANG_46	BRONZE	8969289	0.709999979
BARANG_37	BRONZE	3043762	0.850000024
BARANG_19	SILVER	2421268.5	0.180000007
BARANG_28	SILVER	1036290.44	0.600000024
BARANG_53	GOLD	6493992	0.460000008
BARANG_70	SILVER	383244.031	0.980000019
BARANG_62	BRONZE	9740794	0.839999974
BARANG_96	BRONZE	3783160	0.949999988
BARANG_43	SILVER	7889484	0.899999976
BARANG_19	BRONZE	3140687	0.529999971
BARANG_65	PLATINUM	5235785	0.810000002
BARANG_35	BRONZE	1557154.75	0.119999997
BARANG_34	SILVER	8531567	0.370000005
BARANG_67	GOLD	8484124	0.540000021
BARANG_27	BRONZE	6110603	0.889999986
BARANG_45	BRONZE	6962054.5	0.879999995
BARANG_5	BRONZE	3423799.75	0.99000001
BARANG_38	SILVER	3396326.5	0.579999983
BARANG_22	BRONZE	2578845.25	0.180000007
BARANG_75	GOLD	7323424	0.419999987
BARANG_23	GOLD	8230093	0.550000012
BARANG_98	PLATINUM	9652271	0.569999993
BARANG_15	PLATINUM	9682597	0.540000021
BARANG_67	SILVER	2456859.75	0.150000006
BARANG_79	BRONZE	2054632	0.0299999993
BARANG_12	SILVER	8078209.5	0.330000013
BARANG_24	SILVER	4726914	0.400000006
BARANG_34	PLATINUM	9179514	0.75999999
BARANG_55	GOLD	6118360	0.519999981
BARANG_22	PLATINUM	8596912	0.649999976
BARANG_3	GOLD	7976258	0.800000012
BARANG_96	PLATINUM	1074261.25	0.819999993
BARANG_16	PLATINUM	4359869	0.75
BARANG_87	BRONZE	5967441.5	0.529999971
BARANG_77	GOLD	2761586.25	0.400000006
BARANG_71	BRONZE	2339986.25	0.0199999996
BARANG_88	BRONZE	8022719	0.409999996
BARANG_11	PLATINUM	4691211.5	0.600000024
BARANG_97	BRONZE	6323396	0.779999971
BARANG_12	BRONZE	2961621	0.790000021
BARANG_30	GOLD	9454658	0.409999996
BARANG_28	GOLD	5021313	0.419999987
BARANG_48	PLATINUM	5261948	0.649999976
BARANG_57	BRONZE	7601119	0.0500000007
BARANG_58	SILVER	5620264	0.460000008
BARANG_80	SILVER	72832.0234	0.879999995
BARANG_19	PLATINUM	9624636	0.899999976
BARANG_61	BRONZE	3038879.5	0.230000004
BARANG_89	SILVER	8982287	0.610000014
BARANG_5	PLATINUM	2270117	0.0399999991
BARANG_72	PLATINUM	3972742.75	0.180000007
BARANG_30	BRONZE	3453657.25	0.819999993
BARANG_42	GOLD	907634.188	0.75999999
BARANG_2	PLATINUM	3228310	0.970000029
BARANG_15	BRONZE	3911595.5	0.689999998
BARANG_25	PLATINUM	6954473	0.370000005
BARANG_49	BRONZE	9189405	0.0700000003
BARANG_95	BRONZE	6504636	0.449999988
BARANG_70	PLATINUM	3213910	0.949999988
BARANG_18	SILVER	5215118	0.270000011
BARANG_26	BRONZE	4735615	0.430000007
BARANG_72	GOLD	9749259	0.529999971
BARANG_37	PLATINUM	8015989	0.379999995
BARANG_1	GOLD	693303.125	0.699999988
BARANG_93	PLATINUM	7177213	0.829999983
BARANG_50	PLATINUM	6100348	0.50999999
BARANG_95	PLATINUM	9014098	0.219999999
BARANG_29	GOLD	5653299	0.200000003
BARANG_100	BRONZE	6029185	0.119999997
BARANG_51	GOLD	3601684.5	0.819999993
BARANG_46	PLATINUM	3523290	0.920000017
BARANG_74	PLATINUM	6878535	0.75
BARANG_16	BRONZE	8044261	0.49000001
BARANG_92	PLATINUM	3285294.5	0.49000001
BARANG_40	SILVER	9713435	0.230000004
BARANG_54	GOLD	8484319	0.660000026
BARANG_17	BRONZE	3569553.75	0.0900000036
BARANG_7	BRONZE	4640064	0.939999998
BARANG_2	BRONZE	8745990	0.239999995
BARANG_84	SILVER	6073157	0.0199999996
BARANG_100	PLATINUM	5197254	0.670000017
BARANG_31	PLATINUM	6770650	0.649999976
BARANG_26	SILVER	2285982.25	0.0599999987
BARANG_56	SILVER	5636064.5	0.819999993
BARANG_3	BRONZE	8752371	0.620000005
BARANG_52	BRONZE	7641564	0.879999995
BARANG_4	BRONZE	9626223	0.25
BARANG_62	PLATINUM	7489362	0.460000008
BARANG_56	PLATINUM	1430246	0.540000021
BARANG_83	SILVER	3535172	0.839999974
BARANG_51	PLATINUM	5389006.5	0.25
BARANG_47	BRONZE	1736691.5	0.850000024
BARANG_47	SILVER	5361945	0.689999998
BARANG_55	SILVER	4320482.5	0.319999993
BARANG_13	GOLD	2204955	0.639999986
BARANG_3	SILVER	4479780	0.460000008
BARANG_76	GOLD	5450970	0.469999999
BARANG_44	BRONZE	9051281	0.819999993
BARANG_14	PLATINUM	6106676	0.569999993
BARANG_33	PLATINUM	7286645	0.829999983
BARANG_81	PLATINUM	2496690	0.460000008
BARANG_30	PLATINUM	1018458.88	0.0700000003
BARANG_9	GOLD	2956859	0.119999997
BARANG_7	PLATINUM	93220.2031	0.720000029
BARANG_89	GOLD	9898557	0.639999986
BARANG_42	PLATINUM	1527208	0.560000002
BARANG_59	PLATINUM	961925.438	0.289999992
BARANG_91	BRONZE	8504526	0.0700000003
BARANG_48	SILVER	8709500	0.970000029
BARANG_79	PLATINUM	5397248	0.0799999982
BARANG_91	GOLD	4497832	0.959999979
BARANG_24	PLATINUM	5566847	0.0700000003
BARANG_71	SILVER	2245157.5	0.109999999
BARANG_13	BRONZE	8572746	0.479999989
BARANG_100	GOLD	5937241	0.769999981
BARANG_20	GOLD	8926008	0.0700000003
BARANG_60	GOLD	5268015	0.829999983
BARANG_38	GOLD	8363225.5	0.0700000003
BARANG_9	SILVER	4424987	0.569999993
BARANG_73	SILVER	7508568.5	0.689999998
BARANG_57	PLATINUM	9957469	0.209999993
BARANG_8	GOLD	6598334	0.0500000007
BARANG_79	SILVER	4093340.75	0.620000005
BARANG_35	SILVER	2039646.5	0.0399999991
BARANG_50	BRONZE	6210455	0.0199999996
BARANG_10	GOLD	4250122.5	0.99000001
BARANG_68	SILVER	7582931	0.939999998
BARANG_45	SILVER	2078377.62	0.180000007
BARANG_15	SILVER	7107292	0.629999995
BARANG_91	PLATINUM	28229.5195	0.140000001
BARANG_51	SILVER	4384078	0.270000011
BARANG_20	SILVER	2014811.88	0.689999998
BARANG_66	BRONZE	2233181.5	0.5
BARANG_44	SILVER	4972201	0.790000021
BARANG_32	BRONZE	9720834	0.540000021
BARANG_99	SILVER	3750308.75	0.75999999
BARANG_8	PLATINUM	6983541	0.560000002
BARANG_5	GOLD	8116179	0.400000006
BARANG_18	BRONZE	8187642.5	0.540000021
BARANG_70	BRONZE	7415199	0.439999998
BARANG_18	GOLD	1811540.38	0.200000003
BARANG_14	BRONZE	2232551	0.100000001
BARANG_83	GOLD	8046135.5	0.600000024
BARANG_96	GOLD	7917887	0.850000024
BARANG_60	PLATINUM	9480335	0.839999974
BARANG_89	PLATINUM	8306711	0.899999976
BARANG_28	BRONZE	1441984.38	0.469999999
BARANG_58	BRONZE	5099080	0.879999995
BARANG_99	PLATINUM	6758736	0.319999993
BARANG_77	SILVER	6442213.5	0.370000005
BARANG_16	GOLD	9049082	0.860000014
BARANG_63	PLATINUM	9562778	0.579999983
BARANG_49	PLATINUM	5714250	0.280000001
BARANG_49	SILVER	6525507	0.340000004
BARANG_73	BRONZE	5464423	0.870000005
BARANG_34	BRONZE	7058755.5	0.129999995
BARANG_4	PLATINUM	5421855	0.270000011
BARANG_87	SILVER	8144056.5	0.189999998
BARANG_92	GOLD	2053326.5	0.389999986
BARANG_74	SILVER	1018470.06	0.800000012
BARANG_31	SILVER	2814256.75	0.949999988
BARANG_93	SILVER	1523714.25	0.289999992
BARANG_7	SILVER	4188850.75	0.230000004
BARANG_81	BRONZE	2493680.5	0.330000013
BARANG_76	PLATINUM	6479990.5	0.970000029
BARANG_78	SILVER	6119940	0.479999989
BARANG_22	SILVER	6262337.5	0.280000001
BARANG_82	SILVER	8513728	0.790000021
BARANG_25	BRONZE	7249877	0.159999996
BARANG_46	SILVER	8969341	0.170000002
BARANG_63	GOLD	1750175.5	0.790000021
BARANG_10	PLATINUM	4866013	0.560000002
BARANG_36	PLATINUM	4740857	0.360000014
BARANG_33	SILVER	2235723.25	0.959999979
BARANG_80	PLATINUM	425443.312	0.939999998
BARANG_99	BRONZE	9473555	0.829999983
BARANG_62	SILVER	2671034.5	0.610000014
BARANG_44	PLATINUM	8594556	0.0700000003
BARANG_85	SILVER	2234458	0.920000017
BARANG_48	BRONZE	6752686	0.280000001
BARANG_35	GOLD	1484400.25	0.660000026
BARANG_11	BRONZE	5325553	0.400000006
BARANG_88	GOLD	4050614.5	0.209999993
BARANG_39	SILVER	3176576	0.699999988
BARANG_61	SILVER	2452925.75	0.219999999
BARANG_83	PLATINUM	458724.781	0.119999997
BARANG_90	GOLD	2964968	0.280000001
BARANG_78	GOLD	1375060.25	0.430000007
BARANG_11	GOLD	3294643.75	0.319999993
BARANG_88	PLATINUM	2457774	0.289999992
BARANG_17	GOLD	4813740	0.0500000007
BARANG_20	BRONZE	1841270	0.910000026
BARANG_71	PLATINUM	9772446	0.25999999
BARANG_94	BRONZE	7366529	0.560000002
BARANG_27	PLATINUM	6308256.5	0.610000014
BARANG_92	SILVER	9685101	0.860000014
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
item_1	deskripsi_item_1	8	14	bahan_1
item_2	deskripsi_item_2	1	12	bahan_2
item_3	deskripsi_item_3	2	11	bahan_3
item_4	deskripsi_item_4	5	9	bahan_4
item_5	deskripsi_item_5	2	14	bahan_5
item_6	deskripsi_item_6	2	11	bahan_6
item_7	deskripsi_item_7	6	14	bahan_1
item_8	deskripsi_item_8	0	11	bahan_2
item_9	deskripsi_item_9	4	14	bahan_3
item_10	deskripsi_item_10	4	10	bahan_4
item_11	deskripsi_item_11	8	10	bahan_5
item_12	deskripsi_item_12	2	12	bahan_6
item_13	deskripsi_item_13	8	12	bahan_1
item_14	deskripsi_item_14	7	10	bahan_2
item_15	deskripsi_item_15	4	13	bahan_3
item_16	deskripsi_item_16	4	14	bahan_4
item_17	deskripsi_item_17	8	11	bahan_5
item_18	deskripsi_item_18	1	11	bahan_6
item_19	deskripsi_item_19	8	13	bahan_1
item_20	deskripsi_item_20	6	10	bahan_2
item_21	deskripsi_item_21	0	12	bahan_3
item_22	deskripsi_item_22	2	13	bahan_4
item_23	deskripsi_item_23	5	11	bahan_5
item_24	deskripsi_item_24	4	9	bahan_6
item_25	deskripsi_item_25	3	10	bahan_1
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.kategori (nama, level, sub_dari) FROM stdin;
stroller	1	\N
gendongan	2	\N
motorik_halus	3	\N
rumah_rumahan	4	motorik_halus
baby_walker	5	motorik_halus
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.kategori_item (nama_item, nama_kategori) FROM stdin;
item_1	stroller
item_2	stroller
item_3	stroller
item_4	stroller
item_5	stroller
item_6	stroller
item_7	stroller
item_8	stroller
item_9	stroller
item_10	stroller
item_11	stroller
item_12	stroller
item_13	stroller
item_14	stroller
item_15	stroller
item_16	stroller
item_17	stroller
item_18	stroller
item_19	stroller
item_20	stroller
item_21	stroller
item_22	stroller
item_23	stroller
item_24	stroller
item_25	stroller
item_1	gendongan
item_2	gendongan
item_3	gendongan
item_4	gendongan
item_5	gendongan
item_6	gendongan
item_7	gendongan
item_8	gendongan
item_9	gendongan
item_10	gendongan
item_11	gendongan
item_12	gendongan
item_13	gendongan
item_14	gendongan
item_15	gendongan
item_16	gendongan
item_17	gendongan
item_18	gendongan
item_19	gendongan
item_20	gendongan
item_21	gendongan
item_22	gendongan
item_23	gendongan
item_24	gendongan
item_25	gendongan
item_1	motorik_halus
item_2	motorik_halus
item_3	motorik_halus
item_4	motorik_halus
item_5	motorik_halus
item_6	motorik_halus
item_7	motorik_halus
item_8	motorik_halus
item_9	motorik_halus
item_10	motorik_halus
item_11	motorik_halus
item_12	motorik_halus
item_13	motorik_halus
item_14	motorik_halus
item_15	motorik_halus
item_16	motorik_halus
item_17	motorik_halus
item_18	motorik_halus
item_19	motorik_halus
item_20	motorik_halus
item_21	motorik_halus
item_22	motorik_halus
item_23	motorik_halus
item_24	motorik_halus
item_25	motorik_halus
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
BRONZE	0	Level Terbawah
SILVER	50	Level Menengah Kebawah
GOLD	100	Level Menengah Keatas
PLATINUM	200	Level Teratas
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
order_2	2017-05-06 00:00:00	119	32052.0723	82298.0078	2	sudah_dikembalikan
order_3	2017-05-10 00:00:00	97	56590.8125	70963.9297	3	sudah_dikembalikan
order_4	2017-05-26 00:00:00	189	27410.5625	25475.291	4	sudah_dikembalikan
order_5	2017-06-13 00:00:00	158	65393.9883	34877.9219	5	sudah_dikembalikan
order_6	2017-07-18 00:00:00	126	24350.1016	45708.9766	6	sudah_dikembalikan
order_7	2017-07-30 00:00:00	155	19462.8867	48449.5586	7	sudah_dikembalikan
order_8	2017-08-14 00:00:00	193	20554.9688	13825.2012	8	sudah_dikembalikan
order_9	2017-09-05 00:00:00	195	80628.5625	98081.6484	9	sudah_dikembalikan
order_10	2017-09-25 00:00:00	80	36993.0664	64039.4258	10	sudah_dikembalikan
order_11	2017-11-30 00:00:00	64	30761.9922	18909.2129	11	sudah_dikembalikan
order_12	2018-01-21 00:00:00	39	79036.0781	81212.5234	12	sudah_dikembalikan
order_13	2018-02-24 00:00:00	158	99957.9062	35270.6055	13	batal
order_14	2018-02-27 00:00:00	171	97514.0938	83068.4766	14	batal
order_15	2018-03-04 00:00:00	126	45953.8945	63243.7539	15	sudah_dikembalikan
order_16	2018-03-30 00:00:00	28	85261.125	94127.3672	16	sudah_dikembalikan
order_17	2018-04-12 00:00:00	127	94355.1797	78090.9062	17	sudah_dikembalikan
order_18	2018-04-12 00:00:00	149	99864.4453	65767.6328	18	sedang_dikirim
order_19	2018-04-13 00:00:00	118	73864.6953	94627.9062	19	batal
order_20	2018-04-21 00:00:00	83	43277.3125	33879.4336	20	sudah_dikembalikan
order_21	2018-04-21 00:00:00	106	94465.8438	73927.4688	21	sudah_dikembalikan
order_23	2018-05-20 00:00:00	57	98949.2578	80225.3672	23	sudah_dikembalikan
order_24	2018-05-20 00:00:00	148	14249.457	94858.9766	24	dalam_masa_sewa
order_25	2018-06-12 00:00:00	81	20280.1816	87302.0469	25	dalam_masa_sewa
order_26	2018-06-21 00:00:00	25	11347.3428	11603.7031	26	sudah_dikembalikan
order_27	2018-06-22 00:00:00	133	24237.1602	75430.3906	27	batal
order_28	2018-09-19 00:00:00	93	90291.5078	17824.2324	28	dalam_masa_sewa
order_29	2018-09-28 00:00:00	198	54197.2578	28166.1094	29	dalam_masa_sewa
order_30	2018-10-05 00:00:00	58	97974.8359	67570.5469	30	batal
order_31	2018-10-26 00:00:00	13	53018.2109	71813.3828	31	sudah_dikembalikan
order_32	2018-10-28 00:00:00	175	88904.7812	67006.0234	32	dalam_masa_sewa
order_33	2018-11-01 00:00:00	93	59963.1133	73439.6094	33	sedang_dikirim
order_34	2018-11-04 00:00:00	80	86909.6797	22425.6602	34	sedang_dikirim
order_35	2018-11-05 00:00:00	168	11369.7617	59817.6875	35	dalam_masa_sewa
order_36	2018-11-06 00:00:00	45	91148.2188	74945.7188	36	sudah_dikembalikan
order_37	2018-11-11 00:00:00	57	48963.8438	17810.7051	37	sedang_dikirim
order_38	2018-11-18 00:00:00	2	86228.0391	36657.5625	38	sedang_disiapkan
order_39	2018-11-25 00:00:00	94	87143.2969	80933.9219	39	sedang_dikonfirmasi
order_40	2018-12-04 00:00:00	186	99840.2578	56675.1016	40	sudah_dikembalikan
order_41	2018-12-15 00:00:00	94	66425.5469	59016.8672	41	sudah_dikembalikan
order_42	2018-12-22 00:00:00	52	94817.1484	36981.7305	42	sedang_disiapkan
order_43	2018-12-26 00:00:00	96	62911.1562	40183.1523	43	batal
order_44	2018-12-30 00:00:00	8	41196.4844	82087.6875	44	sedang_disiapkan
order_45	2019-01-07 00:00:00	42	91949.2031	98990.3594	45	sedang_dikonfirmasi
order_46	2019-01-31 00:00:00	89	43930.1367	40181.3047	46	sedang_dikonfirmasi
order_47	2019-02-14 00:00:00	14	24751.4492	10507.0449	47	sedang_disiapkan
order_48	2019-02-17 00:00:00	27	81682.7734	19870.6426	48	sedang_dikirim
order_49	2019-03-23 00:00:00	177	66152.0781	53703.2148	49	sedang_dikirim
order_50	2019-03-24 00:00:00	3	94957.5391	37554.8945	50	batal
order_1	2017-04-24 00:00:00	112	316069.375	22281.9316	1	batal
order_22	2018-05-18 00:00:00	128	99846.3281	48656.0156	22	sudah_dikembalikan
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, nama_alamat_anggota, no_ktp_anggota) FROM stdin;
1001100002	order_2	Pick_up	4000	2017-05-06	alamat_2	2
1001100003	order_3	Pick_up	4000	2017-05-10	alamat_3	3
1001100004	order_4	Pick_up	4000	2017-05-26	alamat_4	4
1001100005	order_5	Pick_up	4000	2017-06-13	alamat_5	5
1001100006	order_6	Self_return	0	2017-07-18	alamat_6	6
1001100007	order_7	Self_return	0	2017-07-30	alamat_7	7
1001100008	order_8	Pick_up	5000	2017-08-14	alamat_8	8
1001100009	order_9	Pick_up	4000	2017-09-05	alamat_9	9
1001100010	order_10	Self_return	0	2017-09-25	alamat_10	10
1001100011	order_11	Pick_up	6000	2017-11-30	alamat_11	11
1001100012	order_12	Pick_up	6000	2018-01-21	alamat_12	12
1001100015	order_15	Self_return	0	2018-03-04	alamat_15	15
1001100016	order_16	Pick_up	5000	2018-03-30	alamat_16	16
1001100017	order_17	Self_return	0	2018-04-12	alamat_17	17
1001100020	order_20	Pick_up	5000	2018-04-21	alamat_20	20
1001100021	order_21	Pick_up	6000	2018-04-21	alamat_21	21
1001100022	order_22	Pick_up	5000	2018-05-18	alamat_22	22
1001100023	order_23	Pick_up	5000	2018-05-20	alamat_23	23
1001100026	order_26	Self_return	0	2018-06-21	alamat_26	26
1001100031	order_31	Pick_up	7000	2018-10-26	alamat_31	31
1001100036	order_36	Self_return	0	2018-11-06	alamat_36	36
1001100040	order_40	Self_return	0	2018-12-04	alamat_40	40
1001100041	order_41	Pick_up	1200	2018-12-15	alamat_41	41
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
1	pengguna_1	pengguna_1@gmail.com	1999-10-11	81234560001
2	pengguna_2	pengguna_2@gmail.com	1999-09-24	81234560002
3	pengguna_3	pengguna_3@gmail.com	1999-08-27	81234560003
4	pengguna_4	pengguna_4@gmail.com	1999-10-09	81234560004
5	pengguna_5	pengguna_5@gmail.com	1999-10-12	81234560005
6	pengguna_6	pengguna_6@gmail.com	1999-08-11	81234560006
7	pengguna_7	pengguna_7@gmail.com	1999-07-20	81234560007
8	pengguna_8	pengguna_8@gmail.com	1999-08-17	81234560008
9	pengguna_9	pengguna_9@gmail.com	1999-09-29	81234560009
10	pengguna_10	pengguna_10@gmail.com	1999-09-28	81234560010
11	pengguna_11	pengguna_11@gmail.com	1999-10-04	81234560011
12	pengguna_12	pengguna_12@gmail.com	1999-08-18	81234560012
13	pengguna_13	pengguna_13@gmail.com	1999-09-14	81234560013
14	pengguna_14	pengguna_14@gmail.com	1999-10-08	81234560014
15	pengguna_15	pengguna_15@gmail.com	1999-08-21	81234560015
16	pengguna_16	pengguna_16@gmail.com	1999-10-01	81234560016
17	pengguna_17	pengguna_17@gmail.com	1999-07-19	81234560017
18	pengguna_18	pengguna_18@gmail.com	1999-07-16	81234560018
19	pengguna_19	pengguna_19@gmail.com	1999-09-28	81234560019
20	pengguna_20	pengguna_20@gmail.com	1999-09-18	81234560020
21	pengguna_21	pengguna_21@gmail.com	1999-10-12	81234560021
22	pengguna_22	pengguna_22@gmail.com	1999-09-24	81234560022
23	pengguna_23	pengguna_23@gmail.com	1999-10-11	81234560023
24	pengguna_24	pengguna_24@gmail.com	1999-08-31	81234560024
25	pengguna_25	pengguna_25@gmail.com	1999-10-06	81234560025
26	pengguna_26	pengguna_26@gmail.com	1999-10-11	81234560026
27	pengguna_27	pengguna_27@gmail.com	1999-09-09	81234560027
28	pengguna_28	pengguna_28@gmail.com	1999-07-12	81234560028
29	pengguna_29	pengguna_29@gmail.com	1999-07-15	81234560029
30	pengguna_30	pengguna_30@gmail.com	1999-08-23	81234560030
31	pengguna_31	pengguna_31@gmail.com	1999-08-12	81234560031
32	pengguna_32	pengguna_32@gmail.com	1999-09-30	81234560032
33	pengguna_33	pengguna_33@gmail.com	1999-08-10	81234560033
34	pengguna_34	pengguna_34@gmail.com	1999-08-30	81234560034
35	pengguna_35	pengguna_35@gmail.com	1999-07-16	81234560035
36	pengguna_36	pengguna_36@gmail.com	1999-08-29	81234560036
37	pengguna_37	pengguna_37@gmail.com	1999-09-29	81234560037
38	pengguna_38	pengguna_38@gmail.com	1999-10-14	81234560038
39	pengguna_39	pengguna_39@gmail.com	1999-10-01	81234560039
40	pengguna_40	pengguna_40@gmail.com	1999-07-18	81234560040
41	pengguna_41	pengguna_41@gmail.com	1999-07-09	81234560041
42	pengguna_42	pengguna_42@gmail.com	1999-08-27	81234560042
43	pengguna_43	pengguna_43@gmail.com	1999-08-14	81234560043
44	pengguna_44	pengguna_44@gmail.com	1999-07-25	81234560044
45	pengguna_45	pengguna_45@gmail.com	1999-08-02	81234560045
46	pengguna_46	pengguna_46@gmail.com	1999-10-06	81234560046
47	pengguna_47	pengguna_47@gmail.com	1999-09-19	81234560047
48	pengguna_48	pengguna_48@gmail.com	1999-09-27	81234560048
49	pengguna_49	pengguna_49@gmail.com	1999-09-09	81234560049
50	pengguna_50	pengguna_50@gmail.com	1999-09-29	81234560050
51	pengguna_51	pengguna_51@gmail.com	1999-10-15	81234560051
52	pengguna_52	pengguna_52@gmail.com	1999-08-13	81234560052
53	pengguna_53	pengguna_53@gmail.com	1999-08-12	81234560053
54	pengguna_54	pengguna_54@gmail.com	1999-08-17	81234560054
55	pengguna_55	pengguna_55@gmail.com	1999-08-06	81234560055
56	pengguna_56	pengguna_56@gmail.com	1999-09-07	81234560056
57	pengguna_57	pengguna_57@gmail.com	1999-07-19	81234560057
58	pengguna_58	pengguna_58@gmail.com	1999-07-19	81234560058
59	pengguna_59	pengguna_59@gmail.com	1999-09-04	81234560059
60	pengguna_60	pengguna_60@gmail.com	1999-09-01	81234560060
61	pengguna_61	pengguna_61@gmail.com	1999-07-29	81234560061
62	pengguna_62	pengguna_62@gmail.com	1999-09-28	81234560062
63	pengguna_63	pengguna_63@gmail.com	1999-09-25	81234560063
64	pengguna_64	pengguna_64@gmail.com	1999-09-15	81234560064
65	pengguna_65	pengguna_65@gmail.com	1999-07-26	81234560065
66	pengguna_66	pengguna_66@gmail.com	1999-08-11	81234560066
67	pengguna_67	pengguna_67@gmail.com	1999-08-23	81234560067
68	pengguna_68	pengguna_68@gmail.com	1999-10-01	81234560068
69	pengguna_69	pengguna_69@gmail.com	1999-08-18	81234560069
70	pengguna_70	pengguna_70@gmail.com	1999-07-30	81234560070
71	pengguna_71	pengguna_71@gmail.com	1999-07-29	81234560071
72	pengguna_72	pengguna_72@gmail.com	1999-08-27	81234560072
73	pengguna_73	pengguna_73@gmail.com	1999-10-02	81234560073
74	pengguna_74	pengguna_74@gmail.com	1999-07-08	81234560074
75	pengguna_75	pengguna_75@gmail.com	1999-08-16	81234560075
76	pengguna_76	pengguna_76@gmail.com	1999-08-24	81234560076
77	pengguna_77	pengguna_77@gmail.com	1999-08-23	81234560077
78	pengguna_78	pengguna_78@gmail.com	1999-09-12	81234560078
79	pengguna_79	pengguna_79@gmail.com	1999-07-14	81234560079
80	pengguna_80	pengguna_80@gmail.com	1999-09-02	81234560080
81	pengguna_81	pengguna_81@gmail.com	1999-09-23	81234560081
82	pengguna_82	pengguna_82@gmail.com	1999-09-03	81234560082
83	pengguna_83	pengguna_83@gmail.com	1999-09-02	81234560083
84	pengguna_84	pengguna_84@gmail.com	1999-10-05	81234560084
85	pengguna_85	pengguna_85@gmail.com	1999-09-02	81234560085
86	pengguna_86	pengguna_86@gmail.com	1999-09-07	81234560086
87	pengguna_87	pengguna_87@gmail.com	1999-08-30	81234560087
88	pengguna_88	pengguna_88@gmail.com	1999-08-18	81234560088
89	pengguna_89	pengguna_89@gmail.com	1999-08-10	81234560089
90	pengguna_90	pengguna_90@gmail.com	1999-10-01	81234560090
91	admin_91	admin_91@gmail.com	1999-10-06	81234560091
92	admin_92	admin_92@gmail.com	1999-07-13	81234560092
93	admin_93	admin_93@gmail.com	1999-09-12	81234560093
94	admin_94	admin_94@gmail.com	1999-10-11	81234560094
95	admin_95	admin_95@gmail.com	1999-09-24	81234560095
96	admin_96	admin_96@gmail.com	1999-09-30	81234560096
97	admin_97	admin_97@gmail.com	1999-08-01	81234560097
98	admin_98	admin_98@gmail.com	1999-08-09	81234560098
99	admin_99	admin_99@gmail.com	1999-08-18	81234560099
100	admin_100	admin_100@gmail.com	1999-07-20	81234560100
102	pengguna_102	pengguna_102@gmail.com	1999-10-07	81234560102
103	pengguna_103	pengguna_103@gmail.com	1999-10-10	81234560103
104	pengguna_104	pengguna_104@gmail.com	1999-08-18	81234560104
105	pengguna_105	pengguna_105@gmail.com	1999-08-21	81234560105
106	pengguna_106	pengguna_106@gmail.com	1999-10-01	81234560106
107	pengguna_107	pengguna_107@gmail.com	1999-10-05	81234560107
108	pengguna_108	pengguna_108@gmail.com	1999-08-20	81234560108
109	pengguna_109	pengguna_109@gmail.com	1999-08-19	81234560109
110	pengguna_110	pengguna_110@gmail.com	1999-10-08	81234560110
101	pengguna_101	pengguna_101@gmail.com	1999-10-02	81234560101
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, nama_alamat_anggota, no_ktp_anggota) FROM stdin;
1001100002	order_2	Delivery	4000	2017-05-06	alamat_2	2
1001100003	order_3	Delivery	4000	2017-05-10	alamat_3	3
1001100004	order_4	Delivery	4000	2017-05-26	alamat_4	4
1001100005	order_5	Delivery	4000	2017-06-13	alamat_5	5
1001100006	order_6	Takeaway	0	2017-07-18	alamat_6	6
1001100007	order_7	Takeaway	0	2017-07-30	alamat_7	7
1001100008	order_8	Delivery	5000	2017-08-14	alamat_8	8
1001100009	order_9	Delivery	4000	2017-09-05	alamat_9	9
1001100010	order_10	Takeaway	0	2017-09-25	alamat_10	10
1001100011	order_11	Delivery	6000	2017-11-30	alamat_11	11
1001100012	order_12	Delivery	6000	2018-01-21	alamat_12	12
1001100015	order_15	Takeaway	0	2018-03-04	alamat_15	15
1001100016	order_16	Delivery	5000	2018-03-30	alamat_16	16
1001100017	order_17	Takeaway	0	2018-04-12	alamat_17	17
1001100018	order_18	Delivery	5000	2018-04-12	alamat_18	18
1001100020	order_20	Delivery	5000	2018-04-21	alamat_20	20
1001100021	order_21	Delivery	6000	2018-04-21	alamat_21	21
1001100022	order_22	Delivery	5000	2018-05-18	alamat_22	22
1001100023	order_23	Delivery	5000	2018-05-20	alamat_23	23
1001100024	order_24	Delivery	7000	2018-05-20	alamat_24	24
1001100025	order_25	Delivery	5000	2018-06-12	alamat_25	25
1001100026	order_26	Takeaway	0	2018-06-21	alamat_26	26
1001100028	order_28	Delivery	5000	2018-09-19	alamat_28	28
1001100029	order_29	Takeaway	0	2018-09-28	alamat_29	29
1001100031	order_31	Delivery	7000	2018-10-26	alamat_31	31
1001100032	order_32	Takeaway	0	2018-10-28	alamat_32	32
1001100033	order_33	Takeaway	0	2018-11-01	alamat_33	33
1001100034	order_34	Delivery	1000	2018-11-04	alamat_34	34
1001100035	order_35	Takeaway	0	2018-11-05	alamat_35	35
1001100036	order_36	Takeaway	0	2018-11-06	alamat_36	36
1001100037	order_37	Takeaway	0	2018-11-11	alamat_37	37
1001100040	order_40	Takeaway	0	2018-12-04	alamat_40	40
1001100041	order_41	Delivery	1200	2018-12-15	alamat_41	41
1001100048	order_48	Delivery	5000	2019-02-17	alamat_48	48
1001100049	order_49	Delivery	5000	2019-03-23	alamat_49	49
666	order_1	Delivery	9	2017-05-05	alamat_1	1
222	order_22	Delivery	5000	2019-03-23	alamat_2	2
223	order_22	Delivery	5000	2019-03-23	alamat_2	2
224	order_22	Delivery	5000	2019-03-23	alamat_2	2
226	order_22	Delivery	5000	2019-03-23	alamat_2	2
227	order_22	Delivery	5000	2019-03-23	alamat_2	2
231	order_22	Delivery	5000	2019-03-23	alamat_2	2
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: db2018013
--

COPY public.status (nama, deskripsi) FROM stdin;
sedang_dikonfirmasi	pesanan sedang dikonfirmasi
sedang_disiapkan	pesanan sedang disiapkan
sedang_dikirim	pesanan sedang dikirim
dalam_masa_sewa	pesanan dalam masa sewa
sudah_dikembalikan	pesanan sudah dikembalikan
batal	pesanan dibatalkan
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: barang_pesanan new_pemesanan_update_kondisi_barang_trigger; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER new_pemesanan_update_kondisi_barang_trigger AFTER INSERT ON public.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE public.update_kondisi_barang();


--
-- Name: level_keanggotaan update_anggota_when_level_keanggotaan_changed; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER update_anggota_when_level_keanggotaan_changed AFTER INSERT OR DELETE OR UPDATE ON public.level_keanggotaan FOR EACH ROW EXECUTE PROCEDURE public.update_for_new_level();


--
-- Name: barang_pesanan update_harga_sewa_trigger; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER update_harga_sewa_trigger AFTER INSERT ON public.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE public.update_harga_sewa();


--
-- Name: anggota update_level_when_poin_changed; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER update_level_when_poin_changed BEFORE INSERT OR UPDATE OF poin, level ON public.anggota FOR EACH ROW EXECUTE PROCEDURE public.update_level();


--
-- Name: pengiriman update_ongkos_trigger; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER update_ongkos_trigger AFTER INSERT ON public.pengiriman FOR EACH ROW EXECUTE PROCEDURE public.update_ongkos();


--
-- Name: barang update_poin_trigger; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER update_poin_trigger AFTER INSERT ON public.barang FOR EACH ROW EXECUTE PROCEDURE public.update_poin();


--
-- Name: pengiriman update_poin_trigger; Type: TRIGGER; Schema: public; Owner: db2018013
--

CREATE TRIGGER update_poin_trigger AFTER INSERT ON public.pengiriman FOR EACH ROW EXECUTE PROCEDURE public.update_poin();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES public.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES public.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES public.pengembalian(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES public.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES public.item(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES public.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES public.status(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES public.admin(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES public.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_item kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES public.item(nama) ON DELETE CASCADE;


--
-- Name: kategori_item kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES public.kategori(nama) ON DELETE CASCADE;


--
-- Name: kategori kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES public.kategori(nama) ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES public.anggota(no_ktp) ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES public.status(nama) ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES public.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES public.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES public.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES public.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018013
--

ALTER TABLE ONLY public.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES public.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

