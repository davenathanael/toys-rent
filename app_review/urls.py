from django.conf.urls import url
from django.urls import include
from .views import index, form_create_review, form_update_review

app_name = 'app_review'

urlpatterns = [
    url(r'^$', index, name='list_review'),
    url(r'form-create/$', form_create_review, name='form_create_review'),
    url(r'form-update/$', form_update_review, name='form_update_review'),
]
