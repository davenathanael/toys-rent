from django.shortcuts import render
from django.db import connection


# Create your views here.
response = {}

def index(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM BARANG_DIKIRIM")
        record = cursor.fetchall()
    response['barang_dikirim'] = record

    return render(request, 'list_review.html', response)

def form_create_review(request):
    return render(request, 'form_create_review.html', response)

def form_update_review(request):
    return render(request, 'form_update_review.html', response)
