from django.urls import path
from .views import index, chatAdmin

app_name = 'chat'

urlpatterns = [
    path('', index, name='index'),
    path('admin', chatAdmin, name='chatAdmin')
]
