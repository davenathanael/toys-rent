"""toysrent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import app_pengiriman.urls as app_pengiriman
import app_review.urls as app_review
import app_pesanan.urls as app_pesanan
import app_level.urls as app_level

urlpatterns = [
    path('admin/', admin.site.urls),
    path('barang/', include('barang.urls')),
    path('pengiriman/', include(app_pengiriman)),
    path('review/', include(app_review)),
    path('pemesanan/', include(app_pesanan)),
    path('level/', include(app_level)),
    path('chat/', include('chat.urls')),
    path('item/', include('item.urls')),
    path('auth/', include('app_auth.urls')),
    path('profil/', include('profil.urls')),
]
