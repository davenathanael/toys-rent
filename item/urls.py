from django.conf.urls import url
from django.urls import include, path
from .views import create_item, list_item, delete_item

urlpatterns = [
    url(r'^create', create_item, name='item'),
    path('delete/<str:id>', delete_item, name='item'),
    path('', list_item, name='item'),
]
