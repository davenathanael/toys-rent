from django.shortcuts import render
from django.db import connection
from app_auth.helpers import login_required, admin_required

# Create your views here.
response = {}

@admin_required
def create_item(request, no_ktp):
    if request.method == 'POST':
        nama_item = request.POST.get("nama_item")
        deskripsi = request.POST.get("deskripsi")
        usia_minimal = request.POST.get("usia_minimal")
        usia_maksimal = request.POST.get("usia_maksimal")
        bahan = request.POST.get("bahan")
        kategori = request.POST.get("kategori")

        error = {}
        if not nama_item:
            error["nama_item"] = "This field cannot be empty"
        if not usia_minimal:
            error["usia_minimal"] = "This field cannot be empty"
        if not usia_maksimal:
            error["usia_maksimal"] = "This field cannot be empty"
        if usia_minimal and usia_maksimal and usia_maksimal < usia_minimal:
            error["usia_maksimal"] = "Usia maksimal can't be less than Usia minimal"
        if error:
            return render(request, 'item/create.html', {'error' : error})        
        
        if not deskripsi:
            deskripsi = None
        if not bahan:
            bahan = None
        if not kategori:
            kategori = None
        # print(kategori)
        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM ITEM WHERE nama = %s", [nama_item]
        )
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["nama_item"] = "Item is already registered"
            return render(request, 'item/create.html', {'error' : error})

        cursor.execute(
            """INSERT INTO ITEM (nama, deskripsi, usia_dari, usia_sampai, bahan) 
            VALUES (%s, %s, %s, %s, %s)""",
            [nama_item, deskripsi, usia_minimal, usia_maksimal, bahan]
        )
        cursor.execute(
            """INSERT INTO KATEGORI_ITEM (nama_item, nama_kategori)
            VALUES (%s, %s)""",
            [nama_item, kategori]
        )
        return list_item(request)
        

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM KATEGORI")
        record = cursor.fetchall()
    response['kategoris'] = record
    return render(request, 'item/create.html', response)


@admin_required
def delete_item(request, no_ktp):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM KATEGORI_ITEM WHERE nama_item='{0}'".format(id))
        cursor.execute("DELETE FROM ITEM WHERE nama='{0}'".format(id))
    return redirect('/item')



def list_item(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ITEM")
        record = cursor.fetchall()
    response['items'] = record

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM KATEGORI_ITEM")
        record = cursor.fetchall()
    response['kategori_items'] = record

    if "is_admin" in request.session.keys() and request.session["is_admin"]:
        response["is_admin"] = True
    else:
        response["is_admin"] = False

    return render(request, 'item/list.html', response)